﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InteropGenerator
{
    public class DOMField
    {
        public bool IsPointer;
        public bool IsReference;
        public bool IsStatic;
        public bool IsConst;
        public string Name;
        public string Type;
    }

    public class DOMProperty
    {
        public string Name;
        public string GetFunction;
        public string SetFunction;
        public string Type;
    }

    public class DOMMethod
    {
        public string Name;
        public string ReturnType;
        public bool IsConstructor;
        public bool IsDestructor;
        public bool IsStatic;
        public bool IsPropertyMethod;
        public bool IsConst;
        public List<DOMField> Parameters = new List<DOMField>();
    }

    public class DOMClass
    {
        public string Name;
        public DOMMethod Destructor;
        public List<string> Inherits;
        public List<DOMMethod> Methods = new List<DOMMethod>();
        public List<DOMField> Fields = new List<DOMField>();
        public List<DOMProperty> Properties = new List<DOMProperty>();
    }

    public class DOM
    {
        public List<DOMClass> Classes = new List<DOMClass>();
    }

}
