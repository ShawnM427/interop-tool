﻿using InteropGenerator.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InteropGenerator
{
    public static class CppGenerator
    {
        private static readonly string CtxName = "ctx";
        private static string DeclSpec { get { return Settings.Default.DeclSpec; } }

        private static string GetNative(DOMClass c, string member) {
            return Settings.Default.NativePrefix + "_" + c.Name + "_" + member;
        }
        
        public static string GenHeader(DOM dom) {
            StringBuilder builder = new StringBuilder();

            if (Settings.Default.Includes != null)
            {
                foreach (string include in Settings.Default.Includes)
                {
                    builder.AppendLine(string.Format("#include \"{0}\"", include));
                }
            }

            builder.AppendLine("");
            builder.AppendLine("#ifdef __cplusplus");
            builder.AppendLine("extern \"C\" {");
            builder.AppendLine("#endif");

            foreach (DOMClass c in dom.Classes)
            {
                builder.AppendLine("//" + c.Name);
                GenClassMemberProxies(c, ref builder);
                builder.AppendLine("");
            }


            builder.AppendLine("#ifdef __cplusplus");
            builder.AppendLine("}");
            builder.AppendLine("#endif");

            return builder.ToString();
        }

        public static string GenImplementation(DOM dom)
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendLine("#include \"Header.h\"");
            builder.AppendLine();

            foreach (DOMClass c in dom.Classes) {
                builder.AppendLine("//" + c.Name);
                GenClassMemberProxyImpls(c, ref builder);
                builder.AppendLine("");
            }
            
            return builder.ToString();
        }

        private static string ExpandArgs(DOMMethod m, string className = null, string ctxName = null)
        {
            string result = "";

            if (ctxName != null) {
                result += m.IsConst ? "const " : "";
                result += className + "* " + ctxName;
                result += m.Parameters.Count > 0 ? ", " : "";
            }

            for(int ix = 0; ix < m.Parameters.Count; ix++)
            {
                DOMField f = m.Parameters[ix];
                result += FieldType(f) + " " + f.Name;

                if (ix < m.Parameters.Count - 1)
                    result += ", ";
            }
            
            return result;
        }

        private static string FieldType(DOMField f)
        {
            string result = f.IsConst ? "const " : "" + f.Type;
            result += f.IsPointer ? "*" : "";
            result += f.IsReference ? "&" : "";
            return result;
        }

        private static string GetFieldGetter(DOMClass c, DOMField f)
        {           
            if (f.IsStatic) {
                return string.Format("{0} {1} {2}()", DeclSpec, FieldType(f), GetNative(c, "Get" + f.Name));
            }
            else {
                return string.Format("{0} {1} {2}({3}* ctx)", DeclSpec, FieldType(f), GetNative(c, "Set" + f.Name), c.Name);
            }
        }
        private static string GetFieldSetter(DOMClass c, DOMField f)
        {
            if (f.IsStatic) {
                return string.Format("{0} void {1}({2} {3})", DeclSpec, GetNative(c, "Set" + f.Name), FieldType(f), f.Name);
            }
            else {
                return string.Format("{0} void {1}({2}* ctx, {3} {4})", DeclSpec, GetNative(c, "Set" + f.Name), c.Name, FieldType(f), f.Name);
            }
        }

        private static string GetMethodProto(DOMClass c, DOMMethod m)
        {
            if (m.IsConstructor)
                return string.Format("{0} {1} {2}({3})", DeclSpec, c.Name + "*", GetNative(c, m.Name), ExpandArgs(m));
            else if (m.IsDestructor)
                return string.Format("{0} void {1}()", DeclSpec, GetNative(c, "_DTOR"));
            else if (m.IsStatic)
                return string.Format("{0} {1} {2}({3})", DeclSpec, m.ReturnType, GetNative(c, m.Name), ExpandArgs(m));
            else
                return string.Format("{0} {1} {2}({3})", DeclSpec, m.ReturnType, GetNative(c, m.Name), ExpandArgs(m, c.Name, CtxName));
        }

        private static string ArgForward(DOMMethod m)
        {
            string result = "";
            for (int ix = 0; ix < m.Parameters.Count; ix++)
            {
                result += m.Parameters[ix].Name;
                if (ix < m.Parameters.Count - 1)
                    result += ",";
            }
            return result;
        }

        private static void GenClassMemberProxies(DOMClass c, ref StringBuilder builder)
        {
            if (c.Destructor != null) {
                builder.AppendLine(GetMethodProto(c, c.Destructor) + ";");
            }

            foreach(DOMMethod m in c.Methods) {
                builder.AppendLine(GetMethodProto(c, m) + ";");
            }

            foreach(DOMField f in c.Fields) {
                if (!f.IsConst) {
                    builder.AppendLine(GetFieldGetter(c, f) + ";");
                    builder.AppendLine(GetFieldSetter(c, f) + ";");
                }
            }
        }

        private static void GenClassMemberProxyImpls(DOMClass c, ref StringBuilder builder) {
            foreach (DOMMethod m in c.Methods) {
                builder.AppendLine(GenMethodForward(c, m));
            }

            foreach (DOMField f in c.Fields) {
                if (!f.IsConst) {
                    builder.AppendLine(GenFieldGetImpl(c, f));
                    builder.AppendLine(GenFieldSetImpl(c, f));
                }
            }
        }

        private static string GenFieldGetImpl(DOMClass c, DOMField f) {
            string result = GetFieldGetter(c, f) + " {";

            if (f.IsStatic)
                result += string.Format("return {0}::{1};", c.Name, f.Name);
            else
                result += string.Format("return {0}->{1};", CtxName, f.Name);

            result += "}";

            return result;
        }

        private static string GenFieldSetImpl(DOMClass c, DOMField f) {
            string result = GetFieldGetter(c, f) + " { ";

            if (f.IsStatic)
                result += string.Format("{0}::{1} = {1};", c.Name, f.Name);
            else
                result += string.Format("{0}->{1} = {1};", CtxName, f.Name);

            result += "}";

            return result;
        }

        private static string GenMethodForward(DOMClass c, DOMMethod m) {
            string result = GetMethodProto(c, m) + " { \r\n";
            if (m.IsConstructor)
                result += string.Format("\treturn new {0}({1});", c.Name, ArgForward(m));
            else if (m.IsDestructor)
                result += string.Format("\tdelete {0};", CtxName);
            else {
                string ret = m.ReturnType == "void" ? "" : "return ";
                if (m.IsStatic)
                    result += string.Format("\t{0}{1}::{2}({3});", ret, c.Name, m.Name, ArgForward(m));
                else
                    result += string.Format("\t{0}{1}->{2}({3});", ret, CtxName, m.Name, ArgForward(m));
            }
            result += "\r\n}";
            return result;
        }
    }
}
