﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace InteropGenerator
{
    public static class CppParser
    {
        static Regex methodFieldMatch = new Regex(@"\s*(?:const)?\s*(?:([^\s&*]+)([&])?([*])?\s*([^,\s=]+)\s*\=?\s*([^,\s])?,?)*");
        static Regex classFinder = new Regex(@"(?:(?:class)\s*([a-zA-Z_][a-zA-Z\d_-]*)\s*:?\s*(?:(?:(?:public)|(?:protected)|(?:private))\s*([a-zA-Z_][a-zA-Z\d_-]*)\s*,?\s*)*)\s*\{((?>\{(?<c>)|[^{}]+|\}(?<-c>))*(?(c)(?!)))\}", RegexOptions.Multiline);
        static Regex publicFinder = new Regex(@"public:\s*([\s\S]+?)(?=(?:protected:)|(?:private:)|(?:};))");
        static Regex methodFinder = new Regex(@"^[ \t]*(?:virtual)?\s*(?!return)(static)?\s*([^\s{}:~]+)\s+([^\.\s~]+)\((.*)\)\s*(?:const)?\s*[{;]", RegexOptions.Multiline);
        static Regex constructorFinder = new Regex(@"^[ \t]*(?!~)([^\.\s]+)\s*\((.*)\)\s*[{;]", RegexOptions.Multiline);
        static Regex destructorFinder = new Regex(@"^[ \t]*\~([^\.\s]+)\s*\((.*)\)\s*[{;]", RegexOptions.Multiline);
        static Regex fieldFinder = new Regex(@"^\s*(?:const)?\s*([^\s]+)\s+([\S]+);$");

        private static readonly string CtxName = "ctx";

        public struct Options {
            public bool GenProperties;
        }

        private static Options myOptions;
        private static readonly Options DefaultOptions = new Options() {
            GenProperties = true
        };

        public static DOM Parse(string input, Options options)
        {
            myOptions = options;
            return __Parse(input);
        }

        public static DOM Parse(string input)
        {
            myOptions = DefaultOptions;
            return __Parse(input);
        }

        private static DOM __Parse(string input)
        {
            DOM dom = new DOM();

            string text = TrimComments(input);

            MatchCollection classes = classFinder.Matches(text);
            foreach (Match c in classes) {
                string className = c.Groups[1].Value;
                List<string> inheritance = new List<string>();
                for(int ix = 0; ix< c.Groups[2].Captures.Count; ix++) {
                    string name = c.Groups[2].Captures[ix].Value;
                    inheritance.Add(name);
                }
                string classImpl = c.Groups[3].Value;
                ParseClass(className, classImpl, inheritance, ref dom);
            }

            return dom;
        }

        private static void GenerateProperties(ref DOMClass context)
        {
            foreach (DOMMethod m in context.Methods)
            {
                if (m.IsConstructor)
                    continue;

                string lName = m.Name.ToLower();
                if (lName.StartsWith("get") || lName.StartsWith("set"))
                {
                    for (int ix = 0; ix < context.Properties.Count; ix++)
                    {
                        DOMProperty p = context.Properties[ix];
                        if (p.Type == m.ReturnType)
                        {
                            m.IsPropertyMethod = true;
                            if (p.Name == m.Name.Remove(0, 3))
                            {
                                p.GetFunction = lName.StartsWith("get") ? m.Name : context.Properties[ix].GetFunction;
                                p.SetFunction = lName.StartsWith("set") ? m.Name : context.Properties[ix].SetFunction;
                                break;
                            }
                        }
                    }
                    DOMProperty result = new DOMProperty();
                    result.Name = m.Name.Remove(0, 3);
                    result.Type = m.ReturnType;
                    result.GetFunction = lName.StartsWith("get") ? m.Name : null;
                    result.SetFunction = lName.StartsWith("set") ? m.Name : null;
                    context.Properties.Add(result);
                }
            }
        }
        
        private static void ParseClass(string name, string impl, List<string> inheritance, ref DOM dom)
        {
            DOMClass result = new DOMClass();
            result.Name = name;
            result.Inherits = inheritance;

            MatchCollection publicBlocks = publicFinder.Matches(impl);
            foreach (Match m in publicBlocks) {
                ParsePublicBlock(m.Groups[1].Value, ref result);
            }

            if (myOptions.GenProperties)
                GenerateProperties(ref result);

            if (result.Destructor == null) {
                DOMMethod destructor = new DOMMethod();
                destructor.IsStatic = false;
                destructor.ReturnType = null;
                destructor.IsDestructor = true;
                destructor.Name = result.Name;
                result.Destructor = destructor;
            }

            dom.Classes.Add(result);
        }

        private static void ParsePublicBlock(string impl, ref DOMClass result)
        {

            MatchCollection fields = fieldFinder.Matches(impl);
            foreach (Match m in fields) {
                DOMField field = new DOMField();
                field.Name = m.Groups[1].Value;
                field.Type = m.Groups[2].Value;
                result.Fields.Add(field);
            }

            MatchCollection ctors = constructorFinder.Matches(impl);
            foreach (Match m in ctors)
            {
                DOMMethod method = new DOMMethod();
                method.ReturnType = "";
                method.IsConstructor = true;
                method.Name = m.Groups[1].Value;
                ParseMethodArgs(m.Groups[2].Value, ref method);
                result.Methods.Add(method);
            }

            Match destructors = destructorFinder.Match(impl);
            if (destructors.Success)
            {
                DOMMethod destructor = new DOMMethod();
                destructor.IsStatic = false;
                destructor.ReturnType = null;
                destructor.IsDestructor = true;
                destructor.Name = destructors.Groups[1].Value;
                result.Destructor = destructor;
            }

            MatchCollection methods = methodFinder.Matches(impl);
            foreach (Match m in methods) {
                DOMMethod method = new DOMMethod();
                method.IsStatic = !string.IsNullOrWhiteSpace(m.Groups[1].Value);
                method.ReturnType = m.Groups[2].Value;
                method.Name = m.Groups[3].Value;
                ParseMethodArgs(m.Groups[4].Value, ref method);
                result.Methods.Add(method);
            }

        }

        private static void ParseMethodArgs(string value, ref DOMMethod method)
        {
            MatchCollection fields = methodFieldMatch.Matches(value);
            foreach (Match m in fields)
            {
                if (!string.IsNullOrWhiteSpace(m.Groups[4].Value))
                {
                    DOMField field = new DOMField();
                    field.Name = m.Groups[4].Value;
                    field.IsPointer = !string.IsNullOrWhiteSpace(m.Groups[3].Value);
                    field.IsReference = !string.IsNullOrWhiteSpace(m.Groups[2].Value);
                    field.Type = m.Groups[1].Value;
                    if (field.Name == CtxName)
                        field.Name = CtxName + "1";
                    method.Parameters.Add(field);
                }
            }
        }

        private static string TrimComments(string input)
        {
            Regex commentFinder = new Regex(@"(?:\/\*([\s\S]*)\*\/)|(?:\/\/([^\n]*))");
            MatchCollection comments = commentFinder.Matches(input);
            foreach (Match m in comments)
            {
                input = input.Replace(m.Value, "");
            }
            return input;
        }
    }
}
