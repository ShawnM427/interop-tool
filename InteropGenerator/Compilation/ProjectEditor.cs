﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InteropGenerator
{
    public static class ProjectEditor
    {
        public class Project {
            private List<string> mySource;

            private int myInsertionIndex;
            private string myFileName;

            public Project(string fileName) {
                myFileName = fileName;

                mySource = File.ReadAllLines(fileName).ToList();
                __CalcInsertionIndex();
            }

            private void __CalcInsertionIndex() {
                for (int ix = 0; ix < mySource.Count; ix++) {
                    if (mySource[ix].Trim().StartsWith("<!-->Start of tool managed<!-->")) {
                        myInsertionIndex = ix + 2;
                        return;
                    }
                }
                for (int ix = 0; ix < mySource.Count; ix++)
                {
                    if (mySource[ix].Trim().StartsWith("<Project")) {
                        ix++;
                        mySource.Insert(ix, "<!-->Start of tool managed<!-->"); ix++;
                        mySource.Insert(ix, "<ItemGroup>"); ix++;
                        mySource.Insert(ix, "</ItemGroup>"); myInsertionIndex = ix; ix++;
                        mySource.Insert(ix , "<!-->End of tool managed<!-->");                        
                        return;
                    }
                }
            }

            public void InsertCompileUnit(string fileName) {
                mySource.Insert(myInsertionIndex, string.Format("<ClInclude Include=\"{0}\" />", fileName)); myInsertionIndex++;
            }

            public void Save() {
                File.WriteAllLines(myFileName, mySource.ToArray());
            }
        }


        public static Project OpenProject(string path) {
            return new Project(path);
        }
    }
}
