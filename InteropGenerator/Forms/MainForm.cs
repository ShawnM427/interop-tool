﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.CodeDom;
using System.Runtime.InteropServices;
using System.CodeDom.Compiler;
using System.Reflection;
using System.IO;
using InteropGenerator.Properties;

namespace InteropGenerator
{
    public partial class frmMain : ThemedForm
    {
        public frmMain()
        {
            DoubleBuffered = true;

            InitializeComponent();
                        
            txtCppSource.BindTabSize(Settings.Default, "TabSize");
            txtCsWrapper.BindTabSize(Settings.Default, "TabSize");
            txtCppWrapperH.BindTabSize(Settings.Default, "TabSize");
            txtCppWrapperC.BindTabSize(Settings.Default, "TabSize");

            {
                ContextMenu cppMenu = new ContextMenu();
                MenuItem item = new MenuItem("Export to Project");
                item.Click += CppExportToProject;
                cppMenu.MenuItems.Add(item);

                lblCppWrapperHeader.ContextMenu = cppMenu;
            }

            {
                ContextMenu csMenu = new ContextMenu();
                MenuItem item = new MenuItem("Save");
                item.Click += CsSaveFile;
                csMenu.MenuItems.Add(item);

                lblCsWrapperHeader.ContextMenu = csMenu;
            }
            
            PropegateBaseTheme(this);
            
            //hScrollSource.ValueChanged += HScrollSource_ValueChanged;
        }

        private void CsSaveFile(object sender, EventArgs e) {
            if (dlgSaveCs.ShowDialog() == DialogResult.OK) {
                File.WriteAllText(dlgSaveCs.FileName, txtCsWrapper.Text);
            }
        }

        private void CppExportToProject(object sender, EventArgs e) {
            if (dlgOpenNative.ShowDialog() == DialogResult.OK) {
                string fileName = "";
                if (TextModal.Show("Enter file name", "File name:", out fileName) == DialogResult.OK) {
                    __GenNativeProjectFiles(dlgOpenNative.FileName, fileName);
                }
            }
        }

        private void __GenNativeProject(string path) {
            File.WriteAllText(path, Resources.NativeProjectPrefab);
            if(!Directory.Exists(Path.Combine(Path.GetDirectoryName(path), "src")))
                Directory.CreateDirectory(Path.Combine(Path.GetDirectoryName(path), "src"));
            File.WriteAllText(Path.Combine(Path.GetDirectoryName(path), "src", "LibSettings.h"), Resources.NativeLibSettingsH);
            File.WriteAllText(Path.Combine(Path.GetDirectoryName(path), "src", "DebugTests.cpp"), Resources.NativeDebugPrefab);

        }
             
        private void __GenNativeProjectFiles(string projectPath, string fileName) {
            if (!Directory.Exists(Path.Combine(Path.GetDirectoryName(projectPath), "src")))
                Directory.CreateDirectory(Path.Combine(Path.GetDirectoryName(projectPath), "src"));

            File.WriteAllText(Path.Combine(Path.GetDirectoryName(projectPath), "src", fileName + ".h"), txtCppWrapperH.Text);
            File.WriteAllText(Path.Combine(Path.GetDirectoryName(projectPath), "src", fileName + ".cpp"), txtCppWrapperC.Text);

            var p = ProjectEditor.OpenProject(projectPath);
            p.InsertCompileUnit(Path.Combine("src", fileName + ".h"));
            p.InsertCompileUnit(Path.Combine("src", fileName + ".cpp"));
            p.Save();

            ThemedMessageBox.Show("Success", "Generated files, please reload project", MessageBoxButtons.OK);
        }

        private void tsbGenerate_Click(object sender, EventArgs e)
        {
            DOM dom = CppParser.Parse(txtCppSource.Text);

            txtCsWrapper.Text = CsGenerator.GenerateDOMInterop(dom);
            txtCppWrapperH.Text = CppGenerator.GenHeader(dom);
            txtCppWrapperC.Text = CppGenerator.GenImplementation(dom);
        }

        private void tsbSettings_Click(object sender, EventArgs e)
        {
            SettingsDialogue dlg = new SettingsDialogue();
            dlg.ShowDialog();
        }

        private void tsiOpen_Click(object sender, EventArgs e)
        {
            if(dlgOpenFile.ShowDialog() == DialogResult.OK) {

                txtCsWrapper.Text   = "";
                txtCppWrapperH.Text = "";
                txtCppWrapperC.Text = "";

                txtCppSource.Text = File.ReadAllText(dlgOpenFile.FileName);
            }
        }

        private void btnClose_Click(object sender, EventArgs e) {
            Close();
        }
                
        private void btnState_Click(object sender, EventArgs e)
        {
            WindowState = WindowState == FormWindowState.Normal ? FormWindowState.Maximized : FormWindowState.Normal;
        }

        private void tsiGenNativeProject_Click(object sender, EventArgs e)
        {
            if (dlgSaveNative.ShowDialog() == DialogResult.OK) {
                __GenNativeProject(dlgSaveNative.FileName);
            }
        }
    }
}
