﻿namespace InteropGenerator.Properties
{
    partial class SettingsDialogue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlBody = new System.Windows.Forms.SplitContainer();
            this.grpProject = new System.Windows.Forms.GroupBox();
            this.pnlIncludes = new System.Windows.Forms.Panel();
            this.txtIncludes = new InteropGenerator.CustomTextBox();
            this.lblIncludes = new System.Windows.Forms.Label();
            this.pnlDeclSpec = new System.Windows.Forms.Panel();
            this.txtDeclSpec = new System.Windows.Forms.TextBox();
            this.lblDeclSec = new System.Windows.Forms.Label();
            this.grpAppSettings = new System.Windows.Forms.GroupBox();
            this.pnlTypeMappings = new System.Windows.Forms.Panel();
            this.dgvTypeMap = new System.Windows.Forms.DataGridView();
            this.clmCpp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmCs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblTypeMappings = new System.Windows.Forms.Label();
            this.pnlNativePrefix = new System.Windows.Forms.Panel();
            this.txtNativePrefix = new System.Windows.Forms.TextBox();
            this.lblNativePrefix = new System.Windows.Forms.Label();
            this.pnlTabSize = new System.Windows.Forms.Panel();
            this.nudTabSize = new InteropGenerator.ThemedNumericUpDown();
            this.lblTabSize = new System.Windows.Forms.Label();
            this.pnlTheme = new System.Windows.Forms.Panel();
            this.cmbThemes = new InteropGenerator.ThemedComboBox();
            this.lblSpacer = new System.Windows.Forms.Label();
            this.btnThemeEdit = new System.Windows.Forms.Button();
            this.lblTheme = new System.Windows.Forms.Label();
            this.pnlFooter = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.cttInfo = new InteropGenerator.CustomTooltip();
            this.pnlContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBody)).BeginInit();
            this.pnlBody.Panel1.SuspendLayout();
            this.pnlBody.Panel2.SuspendLayout();
            this.pnlBody.SuspendLayout();
            this.grpProject.SuspendLayout();
            this.pnlIncludes.SuspendLayout();
            this.pnlDeclSpec.SuspendLayout();
            this.grpAppSettings.SuspendLayout();
            this.pnlTypeMappings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTypeMap)).BeginInit();
            this.pnlNativePrefix.SuspendLayout();
            this.pnlTabSize.SuspendLayout();
            this.pnlTheme.SuspendLayout();
            this.pnlFooter.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlContent
            // 
            this.pnlContent.Controls.Add(this.pnlBody);
            this.pnlContent.Controls.Add(this.pnlFooter);
            this.pnlContent.Location = new System.Drawing.Point(4, 27);
            this.pnlContent.Size = new System.Drawing.Size(548, 419);
            this.cttInfo.SetTitle(this.pnlContent, null);
            this.cttInfo.SetToolTip(this.pnlContent, null);
            // 
            // pnlHeader
            // 
            this.pnlHeader.Location = new System.Drawing.Point(4, 3);
            this.pnlHeader.Size = new System.Drawing.Size(548, 24);
            this.cttInfo.SetTitle(this.pnlHeader, null);
            this.cttInfo.SetToolTip(this.pnlHeader, null);
            // 
            // pnlBody
            // 
            this.pnlBody.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBody.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.pnlBody.Location = new System.Drawing.Point(0, 0);
            this.pnlBody.Name = "pnlBody";
            this.pnlBody.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // pnlBody.Panel1
            // 
            this.pnlBody.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.pnlBody.Panel1.Controls.Add(this.grpProject);
            this.pnlBody.Panel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.pnlBody.Panel1.Padding = new System.Windows.Forms.Padding(4);
            this.cttInfo.SetTitle(this.pnlBody.Panel1, "Additional #include directives for C++ header");
            this.cttInfo.SetToolTip(this.pnlBody.Panel1, null);
            // 
            // pnlBody.Panel2
            // 
            this.pnlBody.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.pnlBody.Panel2.Controls.Add(this.grpAppSettings);
            this.pnlBody.Panel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.pnlBody.Panel2.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.pnlBody.Panel2.Padding = new System.Windows.Forms.Padding(4);
            this.cttInfo.SetTitle(this.pnlBody.Panel2, "Additional #include directives for C++ header");
            this.cttInfo.SetToolTip(this.pnlBody.Panel2, null);
            this.pnlBody.Size = new System.Drawing.Size(546, 394);
            this.pnlBody.SplitterDistance = 158;
            this.pnlBody.TabIndex = 0;
            this.cttInfo.SetTitle(this.pnlBody, "Additional #include directives for C++ header");
            this.cttInfo.SetToolTip(this.pnlBody, null);
            // 
            // grpProject
            // 
            this.grpProject.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.grpProject.Controls.Add(this.pnlIncludes);
            this.grpProject.Controls.Add(this.pnlDeclSpec);
            this.grpProject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpProject.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.grpProject.Location = new System.Drawing.Point(4, 4);
            this.grpProject.Name = "grpProject";
            this.grpProject.Padding = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.grpProject.Size = new System.Drawing.Size(538, 150);
            this.grpProject.TabIndex = 0;
            this.grpProject.TabStop = false;
            this.grpProject.Text = "Project Settings";
            this.cttInfo.SetTitle(this.grpProject, "Additional #include directives for C++ header");
            this.cttInfo.SetToolTip(this.grpProject, null);
            // 
            // pnlIncludes
            // 
            this.pnlIncludes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.pnlIncludes.Controls.Add(this.txtIncludes);
            this.pnlIncludes.Controls.Add(this.lblIncludes);
            this.pnlIncludes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlIncludes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.pnlIncludes.Location = new System.Drawing.Point(3, 39);
            this.pnlIncludes.Name = "pnlIncludes";
            this.pnlIncludes.Padding = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.pnlIncludes.Size = new System.Drawing.Size(532, 108);
            this.pnlIncludes.TabIndex = 1;
            this.cttInfo.SetTitle(this.pnlIncludes, "Additional #include directives for C++ header");
            this.cttInfo.SetToolTip(this.pnlIncludes, null);
            // 
            // txtIncludes
            // 
            this.txtIncludes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.txtIncludes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIncludes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIncludes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
            this.txtIncludes.GhostAlignment = System.Drawing.StringAlignment.Near;
            this.txtIncludes.GhostText = null;
            this.txtIncludes.InnerPadding = new System.Windows.Forms.Padding(4);
            this.txtIncludes.Lines = new string[0];
            this.txtIncludes.Location = new System.Drawing.Point(83, 6);
            this.txtIncludes.Name = "txtIncludes";
            this.txtIncludes.ReadOnly = false;
            this.txtIncludes.Size = new System.Drawing.Size(449, 102);
            this.txtIncludes.TabIndex = 2;
            this.cttInfo.SetTitle(this.txtIncludes, "Additional #include directives for C++ header");
            this.cttInfo.SetToolTip(this.txtIncludes, null);
            // 
            // lblIncludes
            // 
            this.lblIncludes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.lblIncludes.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblIncludes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lblIncludes.Location = new System.Drawing.Point(0, 6);
            this.lblIncludes.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.lblIncludes.Name = "lblIncludes";
            this.lblIncludes.Size = new System.Drawing.Size(83, 102);
            this.lblIncludes.TabIndex = 1;
            this.lblIncludes.Text = "Additional Includes:";
            this.lblIncludes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cttInfo.SetTitle(this.lblIncludes, "Additional #include directives for C++ header");
            this.cttInfo.SetToolTip(this.lblIncludes, "Example:\r\n\r\nStuff.h\r\nOther.h\r\n-- Becomes --\r\n#include \"Stuff.h\"\r\n#include \"Other." +
        "h\"\r\n\r\n\r\n");
            // 
            // pnlDeclSpec
            // 
            this.pnlDeclSpec.AutoSize = true;
            this.pnlDeclSpec.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.pnlDeclSpec.Controls.Add(this.txtDeclSpec);
            this.pnlDeclSpec.Controls.Add(this.lblDeclSec);
            this.pnlDeclSpec.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlDeclSpec.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.pnlDeclSpec.Location = new System.Drawing.Point(3, 19);
            this.pnlDeclSpec.MinimumSize = new System.Drawing.Size(0, 20);
            this.pnlDeclSpec.Name = "pnlDeclSpec";
            this.pnlDeclSpec.Size = new System.Drawing.Size(532, 20);
            this.pnlDeclSpec.TabIndex = 0;
            this.cttInfo.SetTitle(this.pnlDeclSpec, "Additional #include directives for C++ header");
            this.cttInfo.SetToolTip(this.pnlDeclSpec, null);
            // 
            // txtDeclSpec
            // 
            this.txtDeclSpec.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.txtDeclSpec.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDeclSpec.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDeclSpec.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.txtDeclSpec.Location = new System.Drawing.Point(83, 0);
            this.txtDeclSpec.Name = "txtDeclSpec";
            this.txtDeclSpec.Size = new System.Drawing.Size(449, 20);
            this.txtDeclSpec.TabIndex = 1;
            this.txtDeclSpec.Text = global::InteropGenerator.Properties.Settings.Default.DeclSpec;
            this.cttInfo.SetTitle(this.txtDeclSpec, "Additional #include directives for C++ header");
            this.cttInfo.SetToolTip(this.txtDeclSpec, null);
            // 
            // lblDeclSec
            // 
            this.lblDeclSec.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.lblDeclSec.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblDeclSec.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lblDeclSec.Location = new System.Drawing.Point(0, 0);
            this.lblDeclSec.Name = "lblDeclSec";
            this.lblDeclSec.Size = new System.Drawing.Size(83, 20);
            this.lblDeclSec.TabIndex = 0;
            this.lblDeclSec.Text = "DeclSpec:";
            this.lblDeclSec.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cttInfo.SetTitle(this.lblDeclSec, "Sets the macro for the __declspec(dllexport)");
            this.cttInfo.SetToolTip(this.lblDeclSec, null);
            // 
            // grpAppSettings
            // 
            this.grpAppSettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.grpAppSettings.Controls.Add(this.pnlTypeMappings);
            this.grpAppSettings.Controls.Add(this.pnlNativePrefix);
            this.grpAppSettings.Controls.Add(this.pnlTabSize);
            this.grpAppSettings.Controls.Add(this.pnlTheme);
            this.grpAppSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpAppSettings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.grpAppSettings.Location = new System.Drawing.Point(4, 4);
            this.grpAppSettings.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.grpAppSettings.Name = "grpAppSettings";
            this.grpAppSettings.Size = new System.Drawing.Size(538, 224);
            this.grpAppSettings.TabIndex = 0;
            this.grpAppSettings.TabStop = false;
            this.grpAppSettings.Text = "Application Settings";
            this.cttInfo.SetTitle(this.grpAppSettings, "Additional #include directives for C++ header");
            this.cttInfo.SetToolTip(this.grpAppSettings, null);
            // 
            // pnlTypeMappings
            // 
            this.pnlTypeMappings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.pnlTypeMappings.Controls.Add(this.dgvTypeMap);
            this.pnlTypeMappings.Controls.Add(this.lblTypeMappings);
            this.pnlTypeMappings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTypeMappings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.pnlTypeMappings.Location = new System.Drawing.Point(3, 60);
            this.pnlTypeMappings.Name = "pnlTypeMappings";
            this.pnlTypeMappings.Padding = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.pnlTypeMappings.Size = new System.Drawing.Size(532, 133);
            this.pnlTypeMappings.TabIndex = 2;
            this.cttInfo.SetTitle(this.pnlTypeMappings, "Additional #include directives for C++ header");
            this.cttInfo.SetToolTip(this.pnlTypeMappings, null);
            // 
            // dgvTypeMap
            // 
            this.dgvTypeMap.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTypeMap.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmCpp,
            this.clmCs});
            this.dgvTypeMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTypeMap.Location = new System.Drawing.Point(83, 6);
            this.dgvTypeMap.Name = "dgvTypeMap";
            this.dgvTypeMap.Size = new System.Drawing.Size(449, 127);
            this.dgvTypeMap.TabIndex = 2;
            this.cttInfo.SetTitle(this.dgvTypeMap, null);
            this.cttInfo.SetToolTip(this.dgvTypeMap, null);
            // 
            // clmCpp
            // 
            this.clmCpp.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clmCpp.HeaderText = "C++ Type";
            this.clmCpp.Name = "clmCpp";
            // 
            // clmCs
            // 
            this.clmCs.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clmCs.HeaderText = "C# Type";
            this.clmCs.Name = "clmCs";
            // 
            // lblTypeMappings
            // 
            this.lblTypeMappings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.lblTypeMappings.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblTypeMappings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lblTypeMappings.Location = new System.Drawing.Point(0, 6);
            this.lblTypeMappings.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.lblTypeMappings.Name = "lblTypeMappings";
            this.lblTypeMappings.Size = new System.Drawing.Size(83, 127);
            this.lblTypeMappings.TabIndex = 1;
            this.lblTypeMappings.Text = "Type Mappings (c++,c#)     ";
            this.lblTypeMappings.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cttInfo.SetTitle(this.lblTypeMappings, "Sets the mappings for C++ types to C# types");
            this.cttInfo.SetToolTip(this.lblTypeMappings, "Example entries:\r\nuint32_t,uint\r\nint,int\r\nbool,bool");
            // 
            // pnlNativePrefix
            // 
            this.pnlNativePrefix.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.pnlNativePrefix.Controls.Add(this.txtNativePrefix);
            this.pnlNativePrefix.Controls.Add(this.lblNativePrefix);
            this.pnlNativePrefix.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlNativePrefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.pnlNativePrefix.Location = new System.Drawing.Point(3, 36);
            this.pnlNativePrefix.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.pnlNativePrefix.MinimumSize = new System.Drawing.Size(0, 20);
            this.pnlNativePrefix.Name = "pnlNativePrefix";
            this.pnlNativePrefix.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.pnlNativePrefix.Size = new System.Drawing.Size(532, 24);
            this.pnlNativePrefix.TabIndex = 1;
            this.cttInfo.SetTitle(this.pnlNativePrefix, "Additional #include directives for C++ header");
            this.cttInfo.SetToolTip(this.pnlNativePrefix, null);
            // 
            // txtNativePrefix
            // 
            this.txtNativePrefix.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.txtNativePrefix.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNativePrefix.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::InteropGenerator.Properties.Settings.Default, "NativePrefix", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txtNativePrefix.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNativePrefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.txtNativePrefix.Location = new System.Drawing.Point(83, 3);
            this.txtNativePrefix.Name = "txtNativePrefix";
            this.txtNativePrefix.Size = new System.Drawing.Size(449, 20);
            this.txtNativePrefix.TabIndex = 1;
            this.txtNativePrefix.Text = global::InteropGenerator.Properties.Settings.Default.NativePrefix;
            this.cttInfo.SetTitle(this.txtNativePrefix, "Additional #include directives for C++ header");
            this.cttInfo.SetToolTip(this.txtNativePrefix, null);
            // 
            // lblNativePrefix
            // 
            this.lblNativePrefix.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.lblNativePrefix.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblNativePrefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lblNativePrefix.Location = new System.Drawing.Point(0, 3);
            this.lblNativePrefix.Name = "lblNativePrefix";
            this.lblNativePrefix.Size = new System.Drawing.Size(83, 18);
            this.lblNativePrefix.TabIndex = 0;
            this.lblNativePrefix.Text = "Native Prefix:";
            this.lblNativePrefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cttInfo.SetTitle(this.lblNativePrefix, "Sets the prefix for native functions on the C# side");
            this.cttInfo.SetToolTip(this.lblNativePrefix, null);
            // 
            // pnlTabSize
            // 
            this.pnlTabSize.AutoSize = true;
            this.pnlTabSize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.pnlTabSize.Controls.Add(this.nudTabSize);
            this.pnlTabSize.Controls.Add(this.lblTabSize);
            this.pnlTabSize.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTabSize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.pnlTabSize.Location = new System.Drawing.Point(3, 16);
            this.pnlTabSize.MinimumSize = new System.Drawing.Size(0, 20);
            this.pnlTabSize.Name = "pnlTabSize";
            this.pnlTabSize.Size = new System.Drawing.Size(532, 20);
            this.pnlTabSize.TabIndex = 3;
            this.cttInfo.SetTitle(this.pnlTabSize, "Additional #include directives for C++ header");
            this.cttInfo.SetToolTip(this.pnlTabSize, null);
            // 
            // nudTabSize
            // 
            this.nudTabSize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.nudTabSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudTabSize.DecimalPlaces = 0;
            this.nudTabSize.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nudTabSize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.nudTabSize.Location = new System.Drawing.Point(83, 0);
            this.nudTabSize.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nudTabSize.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.nudTabSize.Name = "nudTabSize";
            this.nudTabSize.Size = new System.Drawing.Size(449, 20);
            this.nudTabSize.TabIndex = 1;
            this.cttInfo.SetTitle(this.nudTabSize, null);
            this.cttInfo.SetToolTip(this.nudTabSize, null);
            this.nudTabSize.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // lblTabSize
            // 
            this.lblTabSize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.lblTabSize.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblTabSize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lblTabSize.Location = new System.Drawing.Point(0, 0);
            this.lblTabSize.Name = "lblTabSize";
            this.lblTabSize.Size = new System.Drawing.Size(83, 20);
            this.lblTabSize.TabIndex = 0;
            this.lblTabSize.Text = "Tab Size:";
            this.lblTabSize.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cttInfo.SetTitle(this.lblTabSize, "Sets the tab width for the code panels");
            this.cttInfo.SetToolTip(this.lblTabSize, null);
            // 
            // pnlTheme
            // 
            this.pnlTheme.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.pnlTheme.Controls.Add(this.cmbThemes);
            this.pnlTheme.Controls.Add(this.lblSpacer);
            this.pnlTheme.Controls.Add(this.btnThemeEdit);
            this.pnlTheme.Controls.Add(this.lblTheme);
            this.pnlTheme.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlTheme.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.pnlTheme.Location = new System.Drawing.Point(3, 193);
            this.pnlTheme.Margin = new System.Windows.Forms.Padding(0);
            this.pnlTheme.MinimumSize = new System.Drawing.Size(0, 20);
            this.pnlTheme.Name = "pnlTheme";
            this.pnlTheme.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.pnlTheme.Size = new System.Drawing.Size(532, 28);
            this.pnlTheme.TabIndex = 4;
            this.cttInfo.SetTitle(this.pnlTheme, "Additional #include directives for C++ header");
            this.cttInfo.SetToolTip(this.pnlTheme, null);
            // 
            // cmbThemes
            // 
            this.cmbThemes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.cmbThemes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbThemes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmbThemes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbThemes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.cmbThemes.FormattingEnabled = true;
            this.cmbThemes.Items.AddRange(new object[] {
            "Default",
            "Create new..."});
            this.cmbThemes.Location = new System.Drawing.Point(83, 3);
            this.cmbThemes.Name = "cmbThemes";
            this.cmbThemes.Size = new System.Drawing.Size(388, 21);
            this.cmbThemes.TabIndex = 5;
            this.cttInfo.SetTitle(this.cmbThemes, null);
            this.cttInfo.SetToolTip(this.cmbThemes, null);
            this.cmbThemes.SelectedIndexChanged += new System.EventHandler(this.cmbThemes_SelectedIndexChanged);
            // 
            // lblSpacer
            // 
            this.lblSpacer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.lblSpacer.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblSpacer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lblSpacer.Location = new System.Drawing.Point(471, 3);
            this.lblSpacer.Name = "lblSpacer";
            this.lblSpacer.Size = new System.Drawing.Size(10, 22);
            this.lblSpacer.TabIndex = 4;
            this.lblSpacer.Text = " ";
            this.lblSpacer.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.cttInfo.SetTitle(this.lblSpacer, "Sets the prefix for native functions on the C# side");
            this.cttInfo.SetToolTip(this.lblSpacer, null);
            // 
            // btnThemeEdit
            // 
            this.btnThemeEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.btnThemeEdit.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnThemeEdit.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnThemeEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnThemeEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemeEdit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btnThemeEdit.Location = new System.Drawing.Point(481, 3);
            this.btnThemeEdit.Name = "btnThemeEdit";
            this.btnThemeEdit.Size = new System.Drawing.Size(51, 22);
            this.btnThemeEdit.TabIndex = 3;
            this.btnThemeEdit.Text = "Edit";
            this.btnThemeEdit.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cttInfo.SetTitle(this.btnThemeEdit, null);
            this.cttInfo.SetToolTip(this.btnThemeEdit, null);
            this.btnThemeEdit.UseVisualStyleBackColor = false;
            this.btnThemeEdit.Click += new System.EventHandler(this.btnThemeEdit_Click);
            // 
            // lblTheme
            // 
            this.lblTheme.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.lblTheme.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblTheme.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lblTheme.Location = new System.Drawing.Point(0, 3);
            this.lblTheme.Name = "lblTheme";
            this.lblTheme.Size = new System.Drawing.Size(83, 22);
            this.lblTheme.TabIndex = 0;
            this.lblTheme.Text = "Theme:";
            this.lblTheme.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cttInfo.SetTitle(this.lblTheme, "Sets the prefix for native functions on the C# side");
            this.cttInfo.SetToolTip(this.lblTheme, null);
            // 
            // pnlFooter
            // 
            this.pnlFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.pnlFooter.Controls.Add(this.btnCancel);
            this.pnlFooter.Controls.Add(this.btnSave);
            this.pnlFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFooter.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.pnlFooter.Location = new System.Drawing.Point(0, 394);
            this.pnlFooter.Name = "pnlFooter";
            this.pnlFooter.Size = new System.Drawing.Size(546, 23);
            this.pnlFooter.TabIndex = 1;
            this.cttInfo.SetTitle(this.pnlFooter, "Additional #include directives for C++ header");
            this.cttInfo.SetToolTip(this.pnlFooter, null);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btnCancel.Location = new System.Drawing.Point(396, 0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.cttInfo.SetTitle(this.btnCancel, "Additional #include directives for C++ header");
            this.cttInfo.SetToolTip(this.btnCancel, null);
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btnSave.Location = new System.Drawing.Point(471, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.cttInfo.SetTitle(this.btnSave, "Additional #include directives for C++ header");
            this.cttInfo.SetToolTip(this.btnSave, null);
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cttInfo
            // 
            this.cttInfo.BackColor2 = System.Drawing.SystemColors.ControlLightLight;
            this.cttInfo.BorderColor = System.Drawing.SystemColors.ActiveBorder;
            this.cttInfo.OwnerDraw = true;
            this.cttInfo.TextColor = System.Drawing.SystemColors.InfoText;
            this.cttInfo.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Italic);
            this.cttInfo.TitleColor = System.Drawing.SystemColors.ControlText;
            this.cttInfo.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.cttInfo.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // SettingsDialogue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(556, 450);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "SettingsDialogue";
            this.Padding = new System.Windows.Forms.Padding(4, 3, 4, 4);
            this.ShowIcon = false;
            this.Text = "Settings Dialogue";
            this.cttInfo.SetTitle(this, "Additional #include directives for C++ header");
            this.cttInfo.SetToolTip(this, null);
            this.pnlContent.ResumeLayout(false);
            this.pnlBody.Panel1.ResumeLayout(false);
            this.pnlBody.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlBody)).EndInit();
            this.pnlBody.ResumeLayout(false);
            this.grpProject.ResumeLayout(false);
            this.grpProject.PerformLayout();
            this.pnlIncludes.ResumeLayout(false);
            this.pnlDeclSpec.ResumeLayout(false);
            this.pnlDeclSpec.PerformLayout();
            this.grpAppSettings.ResumeLayout(false);
            this.grpAppSettings.PerformLayout();
            this.pnlTypeMappings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTypeMap)).EndInit();
            this.pnlNativePrefix.ResumeLayout(false);
            this.pnlNativePrefix.PerformLayout();
            this.pnlTabSize.ResumeLayout(false);
            this.pnlTheme.ResumeLayout(false);
            this.pnlFooter.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer pnlBody;
        private System.Windows.Forms.GroupBox grpProject;
        private System.Windows.Forms.Panel pnlDeclSpec;
        private System.Windows.Forms.TextBox txtDeclSpec;
        private System.Windows.Forms.Label lblDeclSec;
        private System.Windows.Forms.Panel pnlIncludes;
        private System.Windows.Forms.Label lblIncludes;
        private CustomTextBox txtIncludes;
        private System.Windows.Forms.GroupBox grpAppSettings;
        private System.Windows.Forms.Panel pnlTypeMappings;
        private System.Windows.Forms.Label lblTypeMappings;
        private System.Windows.Forms.Panel pnlNativePrefix;
        private System.Windows.Forms.TextBox txtNativePrefix;
        private System.Windows.Forms.Label lblNativePrefix;
        private System.Windows.Forms.Panel pnlFooter;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private CustomTooltip cttInfo;
        private System.Windows.Forms.Panel pnlTabSize;
        private ThemedNumericUpDown nudTabSize;
        private System.Windows.Forms.Label lblTabSize;
        private System.Windows.Forms.Panel pnlTheme;
        private ThemedComboBox cmbThemes;
        private System.Windows.Forms.Label lblSpacer;
        private System.Windows.Forms.Button btnThemeEdit;
        private System.Windows.Forms.Label lblTheme;
        private System.Windows.Forms.DataGridView dgvTypeMap;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmCpp;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmCs;
    }
}