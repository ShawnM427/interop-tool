﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InteropGenerator
{
    public partial class ThemedMessageBox : ThemedForm
    {
        string __Prompt;
        public string Prompt
        {
            get { return __Prompt; }
            set { __Prompt = lblPrompt.Text = value; }
        }

        public ThemedMessageBox()
        {
            MinimizeBox = false;
            MaximizeBox = false;
            ShowIcon = false;
            InitializeComponent();
        }

        public static DialogResult Show(string caption, string prompt, MessageBoxButtons buttons)
        {
            ThemedMessageBox modal = new ThemedMessageBox();
            modal.Prompt = prompt;
            modal.Text = caption;
            modal.btnOK.Visible = buttons == MessageBoxButtons.OK | buttons == MessageBoxButtons.OKCancel;
            modal.btnCancel.Visible = buttons == MessageBoxButtons.OKCancel | buttons == MessageBoxButtons.RetryCancel | buttons == MessageBoxButtons.YesNoCancel;
            DialogResult dgResult = modal.ShowDialog();
            return dgResult;
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnOK_Click(object sender, EventArgs e) {
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
