﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InteropGenerator
{
    public partial class TextModal : ThemedForm
    {
        public string Value
        {
            get { return txtResult.Text; }
            set { txtResult.Text = value; }
        }
        string __Prompt;
        public string Prompt
        {
            get { return __Prompt; }
            set { __Prompt = lblPrompt.Text = value; }
        }

        public TextModal()
        {
            MinimizeBox = false;
            MaximizeBox = false;
            ShowIcon = false;
            InitializeComponent();
            txtResult.Focus();
        }

        public static DialogResult Show(string caption, string prompt, out string result)
        {
            TextModal modal = new TextModal();
            modal.Prompt = prompt;
            modal.Text = caption;
            DialogResult dgResult = modal.ShowDialog();
            result = modal.Value;
            return dgResult;
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            Close();
        }
    }
}
