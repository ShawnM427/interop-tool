﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InteropGenerator
{
    public partial class ThemeEditor : ThemedForm
    {
        bool isDirty;

        public ThemeEditor()
        {
            InitializeComponent();

            ThemeFactory.ApplyTheme(dgvEntry, Theme.Active);
        }

        protected void __SaveChanges()
        {
            isDirty = false;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (isDirty)
            {
                DialogResult result = MessageBox.Show("Would you like to save these changes?", "You have unsaved changes", MessageBoxButtons.YesNoCancel);

                if (result == DialogResult.Yes)
                    __SaveChanges();
                else if (result == DialogResult.Cancel)
                    e.Cancel = true;
            }
            base.OnClosing(e);
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            __SaveChanges();
            Close();
        }
    }
}
