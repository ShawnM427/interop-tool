﻿namespace InteropGenerator
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dlgOpenFile = new System.Windows.Forms.OpenFileDialog();
            this.pnlRootContainer = new System.Windows.Forms.SplitContainer();
            this.pnlSourceScroll = new System.Windows.Forms.Panel();
            this.txtCppSource = new InteropGenerator.CustomTextBox();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.lblCppHeader = new System.Windows.Forms.Label();
            this.pnlOutputSplit = new System.Windows.Forms.SplitContainer();
            this.pnlCpp = new System.Windows.Forms.SplitContainer();
            this.txtCppWrapperH = new InteropGenerator.CustomTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCppH = new System.Windows.Forms.Label();
            this.txtCppWrapperC = new InteropGenerator.CustomTextBox();
            this.lblCppC = new System.Windows.Forms.Label();
            this.lblCppWrapperHeader = new System.Windows.Forms.Label();
            this.txtCsWrapper = new InteropGenerator.CustomTextBox();
            this.lblCsWrapperHeader = new System.Windows.Forms.Label();
            this.mnuMain = new System.Windows.Forms.MenuStrip();
            this.tsmFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsiOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmGenerateMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.tsiGenNativeProject = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.dlgSaveNative = new System.Windows.Forms.SaveFileDialog();
            this.dlgOpenNative = new System.Windows.Forms.OpenFileDialog();
            this.dlgSaveCs = new System.Windows.Forms.SaveFileDialog();
            this.pnlContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlRootContainer)).BeginInit();
            this.pnlRootContainer.Panel1.SuspendLayout();
            this.pnlRootContainer.Panel2.SuspendLayout();
            this.pnlRootContainer.SuspendLayout();
            this.pnlSourceScroll.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlOutputSplit)).BeginInit();
            this.pnlOutputSplit.Panel1.SuspendLayout();
            this.pnlOutputSplit.Panel2.SuspendLayout();
            this.pnlOutputSplit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlCpp)).BeginInit();
            this.pnlCpp.Panel1.SuspendLayout();
            this.pnlCpp.Panel2.SuspendLayout();
            this.pnlCpp.SuspendLayout();
            this.panel1.SuspendLayout();
            this.mnuMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlContent
            // 
            this.pnlContent.Controls.Add(this.pnlRootContainer);
            this.pnlContent.Controls.Add(this.mnuMain);
            this.pnlContent.Location = new System.Drawing.Point(4, 24);
            this.pnlContent.Size = new System.Drawing.Size(1087, 758);
            // 
            // pnlHeader
            // 
            this.pnlHeader.Location = new System.Drawing.Point(4, 0);
            this.pnlHeader.Size = new System.Drawing.Size(1087, 24);
            // 
            // dlgOpenFile
            // 
            this.dlgOpenFile.Filter = "C++ Headers|*.h|All Files|*.*";
            this.dlgOpenFile.Title = "Select a header file";
            // 
            // pnlRootContainer
            // 
            this.pnlRootContainer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.pnlRootContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRootContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlRootContainer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.pnlRootContainer.Location = new System.Drawing.Point(0, 24);
            this.pnlRootContainer.Name = "pnlRootContainer";
            // 
            // pnlRootContainer.Panel1
            // 
            this.pnlRootContainer.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.pnlRootContainer.Panel1.Controls.Add(this.pnlSourceScroll);
            this.pnlRootContainer.Panel1.Controls.Add(this.btnGenerate);
            this.pnlRootContainer.Panel1.Controls.Add(this.lblCppHeader);
            this.pnlRootContainer.Panel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            // 
            // pnlRootContainer.Panel2
            // 
            this.pnlRootContainer.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.pnlRootContainer.Panel2.Controls.Add(this.pnlOutputSplit);
            this.pnlRootContainer.Panel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.pnlRootContainer.Size = new System.Drawing.Size(1085, 732);
            this.pnlRootContainer.SplitterDistance = 360;
            this.pnlRootContainer.TabIndex = 1;
            // 
            // pnlSourceScroll
            // 
            this.pnlSourceScroll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.pnlSourceScroll.Controls.Add(this.txtCppSource);
            this.pnlSourceScroll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSourceScroll.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.pnlSourceScroll.Location = new System.Drawing.Point(0, 24);
            this.pnlSourceScroll.Margin = new System.Windows.Forms.Padding(0);
            this.pnlSourceScroll.Name = "pnlSourceScroll";
            this.pnlSourceScroll.Size = new System.Drawing.Size(358, 683);
            this.pnlSourceScroll.TabIndex = 4;
            // 
            // txtCppSource
            // 
            this.txtCppSource.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.txtCppSource.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCppSource.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCppSource.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
            this.txtCppSource.GhostAlignment = System.Drawing.StringAlignment.Center;
            this.txtCppSource.GhostText = "<Enter C++ header here>";
            this.txtCppSource.InnerPadding = new System.Windows.Forms.Padding(4);
            this.txtCppSource.Lines = new string[0];
            this.txtCppSource.Location = new System.Drawing.Point(0, 0);
            this.txtCppSource.Margin = new System.Windows.Forms.Padding(0);
            this.txtCppSource.Name = "txtCppSource";
            this.txtCppSource.ReadOnly = false;
            this.txtCppSource.Size = new System.Drawing.Size(358, 683);
            this.txtCppSource.TabIndex = 2;
            // 
            // btnGenerate
            // 
            this.btnGenerate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.btnGenerate.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnGenerate.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnGenerate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGenerate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btnGenerate.Location = new System.Drawing.Point(0, 707);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(358, 23);
            this.btnGenerate.TabIndex = 3;
            this.btnGenerate.Text = "Generate Wrapper";
            this.btnGenerate.UseVisualStyleBackColor = false;
            this.btnGenerate.Click += new System.EventHandler(this.tsbGenerate_Click);
            // 
            // lblCppHeader
            // 
            this.lblCppHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.lblCppHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblCppHeader.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCppHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lblCppHeader.Location = new System.Drawing.Point(0, 0);
            this.lblCppHeader.Name = "lblCppHeader";
            this.lblCppHeader.Size = new System.Drawing.Size(358, 24);
            this.lblCppHeader.TabIndex = 2;
            this.lblCppHeader.Text = "C++ Source";
            this.lblCppHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlOutputSplit
            // 
            this.pnlOutputSplit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.pnlOutputSplit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlOutputSplit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlOutputSplit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.pnlOutputSplit.Location = new System.Drawing.Point(0, 0);
            this.pnlOutputSplit.Name = "pnlOutputSplit";
            // 
            // pnlOutputSplit.Panel1
            // 
            this.pnlOutputSplit.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.pnlOutputSplit.Panel1.Controls.Add(this.pnlCpp);
            this.pnlOutputSplit.Panel1.Controls.Add(this.lblCppWrapperHeader);
            this.pnlOutputSplit.Panel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            // 
            // pnlOutputSplit.Panel2
            // 
            this.pnlOutputSplit.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.pnlOutputSplit.Panel2.Controls.Add(this.txtCsWrapper);
            this.pnlOutputSplit.Panel2.Controls.Add(this.lblCsWrapperHeader);
            this.pnlOutputSplit.Panel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.pnlOutputSplit.Size = new System.Drawing.Size(721, 732);
            this.pnlOutputSplit.SplitterDistance = 359;
            this.pnlOutputSplit.TabIndex = 0;
            // 
            // pnlCpp
            // 
            this.pnlCpp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.pnlCpp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCpp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.pnlCpp.Location = new System.Drawing.Point(0, 24);
            this.pnlCpp.Name = "pnlCpp";
            this.pnlCpp.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // pnlCpp.Panel1
            // 
            this.pnlCpp.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.pnlCpp.Panel1.Controls.Add(this.txtCppWrapperH);
            this.pnlCpp.Panel1.Controls.Add(this.panel1);
            this.pnlCpp.Panel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            // 
            // pnlCpp.Panel2
            // 
            this.pnlCpp.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.pnlCpp.Panel2.Controls.Add(this.txtCppWrapperC);
            this.pnlCpp.Panel2.Controls.Add(this.lblCppC);
            this.pnlCpp.Panel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.pnlCpp.Size = new System.Drawing.Size(357, 706);
            this.pnlCpp.SplitterDistance = 333;
            this.pnlCpp.TabIndex = 5;
            // 
            // txtCppWrapperH
            // 
            this.txtCppWrapperH.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.txtCppWrapperH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCppWrapperH.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCppWrapperH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCppWrapperH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
            this.txtCppWrapperH.GhostAlignment = System.Drawing.StringAlignment.Near;
            this.txtCppWrapperH.GhostText = null;
            this.txtCppWrapperH.InnerPadding = new System.Windows.Forms.Padding(4);
            this.txtCppWrapperH.Lines = new string[0];
            this.txtCppWrapperH.Location = new System.Drawing.Point(0, 0);
            this.txtCppWrapperH.Name = "txtCppWrapperH";
            this.txtCppWrapperH.ReadOnly = true;
            this.txtCppWrapperH.Size = new System.Drawing.Size(357, 313);
            this.txtCppWrapperH.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.panel1.Controls.Add(this.lblCppH);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.panel1.Location = new System.Drawing.Point(0, 313);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(357, 20);
            this.panel1.TabIndex = 5;
            // 
            // lblCppH
            // 
            this.lblCppH.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.lblCppH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCppH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lblCppH.Location = new System.Drawing.Point(0, 0);
            this.lblCppH.Name = "lblCppH";
            this.lblCppH.Size = new System.Drawing.Size(357, 20);
            this.lblCppH.TabIndex = 0;
            this.lblCppH.Text = "^ Header ^";
            this.lblCppH.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCppWrapperC
            // 
            this.txtCppWrapperC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.txtCppWrapperC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCppWrapperC.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCppWrapperC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCppWrapperC.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
            this.txtCppWrapperC.GhostAlignment = System.Drawing.StringAlignment.Near;
            this.txtCppWrapperC.GhostText = null;
            this.txtCppWrapperC.InnerPadding = new System.Windows.Forms.Padding(4);
            this.txtCppWrapperC.Lines = new string[0];
            this.txtCppWrapperC.Location = new System.Drawing.Point(0, 0);
            this.txtCppWrapperC.Name = "txtCppWrapperC";
            this.txtCppWrapperC.ReadOnly = true;
            this.txtCppWrapperC.Size = new System.Drawing.Size(357, 349);
            this.txtCppWrapperC.TabIndex = 5;
            // 
            // lblCppC
            // 
            this.lblCppC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.lblCppC.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblCppC.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lblCppC.Location = new System.Drawing.Point(0, 349);
            this.lblCppC.Name = "lblCppC";
            this.lblCppC.Size = new System.Drawing.Size(357, 20);
            this.lblCppC.TabIndex = 6;
            this.lblCppC.Text = "^ Source ^";
            this.lblCppC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCppWrapperHeader
            // 
            this.lblCppWrapperHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.lblCppWrapperHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblCppWrapperHeader.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCppWrapperHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lblCppWrapperHeader.Location = new System.Drawing.Point(0, 0);
            this.lblCppWrapperHeader.Name = "lblCppWrapperHeader";
            this.lblCppWrapperHeader.Size = new System.Drawing.Size(357, 24);
            this.lblCppWrapperHeader.TabIndex = 4;
            this.lblCppWrapperHeader.Text = "C++ Wrapper";
            this.lblCppWrapperHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCsWrapper
            // 
            this.txtCsWrapper.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.txtCsWrapper.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCsWrapper.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCsWrapper.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
            this.txtCsWrapper.GhostAlignment = System.Drawing.StringAlignment.Near;
            this.txtCsWrapper.GhostText = null;
            this.txtCsWrapper.InnerPadding = new System.Windows.Forms.Padding(4);
            this.txtCsWrapper.Lines = new string[0];
            this.txtCsWrapper.Location = new System.Drawing.Point(0, 24);
            this.txtCsWrapper.Name = "txtCsWrapper";
            this.txtCsWrapper.ReadOnly = true;
            this.txtCsWrapper.Size = new System.Drawing.Size(356, 706);
            this.txtCsWrapper.TabIndex = 3;
            // 
            // lblCsWrapperHeader
            // 
            this.lblCsWrapperHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.lblCsWrapperHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblCsWrapperHeader.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCsWrapperHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lblCsWrapperHeader.Location = new System.Drawing.Point(0, 0);
            this.lblCsWrapperHeader.Name = "lblCsWrapperHeader";
            this.lblCsWrapperHeader.Size = new System.Drawing.Size(356, 24);
            this.lblCsWrapperHeader.TabIndex = 4;
            this.lblCsWrapperHeader.Text = "C# Wrapper";
            this.lblCsWrapperHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mnuMain
            // 
            this.mnuMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.mnuMain.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.mnuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmFile,
            this.tsbSettings});
            this.mnuMain.Location = new System.Drawing.Point(0, 0);
            this.mnuMain.Name = "mnuMain";
            this.mnuMain.Size = new System.Drawing.Size(1085, 24);
            this.mnuMain.TabIndex = 6;
            // 
            // tsmFile
            // 
            this.tsmFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsiOpen,
            this.tsmGenerateMenu});
            this.tsmFile.Name = "tsmFile";
            this.tsmFile.Size = new System.Drawing.Size(37, 20);
            this.tsmFile.Text = "File";
            // 
            // tsiOpen
            // 
            this.tsiOpen.Name = "tsiOpen";
            this.tsiOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.tsiOpen.Size = new System.Drawing.Size(146, 22);
            this.tsiOpen.Text = "Open";
            this.tsiOpen.Click += new System.EventHandler(this.tsiOpen_Click);
            // 
            // tsmGenerateMenu
            // 
            this.tsmGenerateMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsiGenNativeProject});
            this.tsmGenerateMenu.Name = "tsmGenerateMenu";
            this.tsmGenerateMenu.Size = new System.Drawing.Size(146, 22);
            this.tsmGenerateMenu.Text = "Generate";
            // 
            // tsiGenNativeProject
            // 
            this.tsiGenNativeProject.Name = "tsiGenNativeProject";
            this.tsiGenNativeProject.Size = new System.Drawing.Size(148, 22);
            this.tsiGenNativeProject.Text = "Native Project";
            this.tsiGenNativeProject.Click += new System.EventHandler(this.tsiGenNativeProject_Click);
            // 
            // tsbSettings
            // 
            this.tsbSettings.Name = "tsbSettings";
            this.tsbSettings.Size = new System.Drawing.Size(61, 20);
            this.tsbSettings.Text = "Settings";
            this.tsbSettings.Click += new System.EventHandler(this.tsbSettings_Click);
            // 
            // dlgSaveNative
            // 
            this.dlgSaveNative.DefaultExt = "vcxproj";
            this.dlgSaveNative.Filter = "VC Project|*.vcxproj";
            // 
            // dlgOpenNative
            // 
            this.dlgOpenNative.DefaultExt = "vcxproj";
            this.dlgOpenNative.Filter = "VC Project|*.vcxproj";
            // 
            // dlgSaveCs
            // 
            this.dlgSaveCs.CreatePrompt = true;
            this.dlgSaveCs.DefaultExt = "cs";
            this.dlgSaveCs.Filter = "C# File|*.cs";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.ClientSize = new System.Drawing.Size(1095, 786);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.Location = new System.Drawing.Point(0, 0);
            this.MaximizeBox = true;
            this.MinimizeBox = true;
            this.Name = "frmMain";
            this.Padding = new System.Windows.Forms.Padding(4, 0, 4, 4);
            this.ShowIcon = false;
            this.Text = "Interop Generator";
            this.pnlContent.ResumeLayout(false);
            this.pnlContent.PerformLayout();
            this.pnlRootContainer.Panel1.ResumeLayout(false);
            this.pnlRootContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlRootContainer)).EndInit();
            this.pnlRootContainer.ResumeLayout(false);
            this.pnlSourceScroll.ResumeLayout(false);
            this.pnlOutputSplit.Panel1.ResumeLayout(false);
            this.pnlOutputSplit.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlOutputSplit)).EndInit();
            this.pnlOutputSplit.ResumeLayout(false);
            this.pnlCpp.Panel1.ResumeLayout(false);
            this.pnlCpp.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlCpp)).EndInit();
            this.pnlCpp.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.mnuMain.ResumeLayout(false);
            this.mnuMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog dlgOpenFile;
        private System.Windows.Forms.SplitContainer pnlRootContainer;
        private System.Windows.Forms.Label lblCppHeader;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.SplitContainer pnlOutputSplit;
        private System.Windows.Forms.SplitContainer pnlCpp;
        private CustomTextBox txtCppWrapperH;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblCppH;
        private CustomTextBox txtCppWrapperC;
        private System.Windows.Forms.Label lblCppC;
        private System.Windows.Forms.Label lblCppWrapperHeader;
        private CustomTextBox txtCsWrapper;
        private System.Windows.Forms.Label lblCsWrapperHeader;
        private System.Windows.Forms.Panel pnlSourceScroll;
        private CustomTextBox txtCppSource;
        private System.Windows.Forms.MenuStrip mnuMain;
        private System.Windows.Forms.ToolStripMenuItem tsmFile;
        private System.Windows.Forms.ToolStripMenuItem tsiOpen;
        private System.Windows.Forms.ToolStripMenuItem tsbSettings;
        private System.Windows.Forms.ToolStripMenuItem tsmGenerateMenu;
        private System.Windows.Forms.ToolStripMenuItem tsiGenNativeProject;
        private System.Windows.Forms.SaveFileDialog dlgSaveNative;
        private System.Windows.Forms.OpenFileDialog dlgOpenNative;
        private System.Windows.Forms.SaveFileDialog dlgSaveCs;
    }
}

