﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InteropGenerator
{
    public static class ThemeFactory
    {
        private static Dictionary<Type, IThemerRoot> myThemers;
        private static DefaultThemer myDefaultThemer = new DefaultThemer();

        static ThemeFactory() {
            myThemers = new Dictionary<Type, IThemerRoot>();

            foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies()) {
                foreach (Type T in a.GetTypes() ) {
                    if (typeof(IThemerRoot).IsAssignableFrom(T)) {
                        if (!T.IsAbstract)
                        {
                            IThemerRoot instance = (IThemerRoot)Activator.CreateInstance(T);
                            Type t = instance.GetControlType();
                            myThemers.Add(t, instance);
                        }
                    }
                }
            }
        }

        public static void ApplyTheme(Control control, Theme theme) {
            if (myDefaultThemer != null)
            {
                myDefaultThemer.Theme(control, theme);

                if (myThemers.ContainsKey(control.GetType()))
                {
                    myThemers[control.GetType()].Theme(control, theme);
                }
            }
        }
    }
}
