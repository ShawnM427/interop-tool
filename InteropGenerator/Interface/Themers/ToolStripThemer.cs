﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InteropGenerator
{
    class MenuStripThemer : IThemer<MenuStrip>
    {
        private CustomToolStripRenderer myRenderer = new CustomToolStripRenderer();

        protected override void ApplyTheme(MenuStrip control, Theme theme) {
            control.Renderer = myRenderer;
        }
    }

    class ToolStripThemer : IThemer<ToolStrip>
    {
        private CustomToolStripRenderer myRenderer = new CustomToolStripRenderer();

        protected override void ApplyTheme(ToolStrip control, Theme theme) {
            control.Renderer = myRenderer;
        }
    }
}
