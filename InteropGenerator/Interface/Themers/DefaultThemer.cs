﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InteropGenerator
{
    public class DefaultThemer : IThemer<Control>
    {
        protected override void ApplyTheme(Control control, Theme theme) {
            control.BackColor = theme.Colors[control.GetType().Name + ".Back", "Back"];
            control.ForeColor = theme.Colors[control.GetType().Name + ".Fore", "Fore"];
        }
    }
}
