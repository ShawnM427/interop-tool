﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InteropGenerator
{
    public class DataGridViewThemer : IThemer<DataGridView>
    {
        protected override void ApplyTheme(DataGridView control, Theme theme) {
            control.EnableHeadersVisualStyles = false;
            foreach(DataGridViewColumn c in control.Columns) {
                c.HeaderCell.Style = GetHeaderStyle(c.HeaderCell.Style, theme);
                c.DefaultCellStyle = GetCellStyle(c.DefaultCellStyle, theme);

            }
            control.BackgroundColor = theme.Colors["DataGridView.Back", "Back"];
            control.GridColor = theme.Colors["DataGridView.Border", "Border"];
            control.ForeColor = theme.Colors["DataGridView.Fore", "Fore"];
            control.RowHeadersDefaultCellStyle = GetCellStyle(control.RowHeadersDefaultCellStyle, theme);
            control.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            control.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            control.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            control.ColumnHeadersDefaultCellStyle = GetHeaderStyle(control.ColumnHeadersDefaultCellStyle, theme);
        }

        protected DataGridViewCellStyle GetCellStyle(DataGridViewCellStyle prefab, Theme theme)
        {
            prefab.BackColor = theme.Colors["DataGridView.Cell.Back", "Back"];
            prefab.ForeColor = theme.Colors["DataGridView.Cell.Fore", "Fore"];
            prefab.SelectionBackColor = theme.Colors["DataGridView.Cell.Selected.Back", "Selection.Back"];
            prefab.SelectionForeColor = theme.Colors["DataGridView.Cell.Selected.Fore", "Selection.Fore"];

            return prefab;
        }

        protected DataGridViewCellStyle GetHeaderStyle(DataGridViewCellStyle prefab, Theme theme)
        {
            prefab.BackColor = theme.Colors["DataGridView.Header.Back", "BackLight"];
            prefab.ForeColor = theme.Colors["DataGridView.Header.Fore", "Fore"];
            prefab.SelectionBackColor = theme.Colors["DataGridView.Header.Selected.Back", "Selection.Back"];
            prefab.SelectionForeColor = theme.Colors["DataGridView.Header.Selected.Fore", "Selection.Fore"];

            return prefab;
        }
    }
}
