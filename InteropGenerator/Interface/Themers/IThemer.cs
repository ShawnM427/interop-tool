﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InteropGenerator
{
    public interface IThemerRoot {
        void Theme(object control, Theme theme);

        Type GetControlType();
    }

    public abstract class IThemer<Control> : IThemerRoot{
        public Type GetControlType() {
            return typeof(Control);
        }

        public void Theme(object control, Theme theme) {
            if (control is Control)
                ApplyTheme((Control)control, theme);
        }

        protected abstract void ApplyTheme(Control control, Theme theme);
    }
}
