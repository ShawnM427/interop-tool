﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InteropGenerator
{
    class NumericUpDownThemer : IThemer<NumericUpDown>
    {
        protected override void ApplyTheme(NumericUpDown control, Theme theme)
        {
            control.BorderStyle = BorderStyle.FixedSingle;
        }
    }
}
