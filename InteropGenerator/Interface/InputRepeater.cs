﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;

namespace InteropGenerator
{
    class InputRepeater
    {
        private Timer m_timer;
        private int m_initdelay = 1000;
        private int m_repdelay = 400;
        private bool isClicked = false;

        public event EventHandler Fire;

        public InputRepeater() {
            m_timer = new Timer();
            m_timer.Tick += timerproc;
            m_timer.Enabled = false;
        }

        private void timerproc(object o1, EventArgs e1)
        {
            m_timer.Interval = m_repdelay;
            if (isClicked) {
                Fire?.Invoke(this, new EventArgs());
            }

        }

        public void MouseDown(object sender, EventArgs e)
        {
            if (!isClicked)
            {
                isClicked = true;

                m_timer.Interval = m_initdelay;
                m_timer.Enabled = true;
                m_timer.Start();
                Fire?.Invoke(this, e);
            }
        }
               
        public void Released(object sender, MouseEventArgs e)
        {
            m_timer.Stop();
            m_timer.Enabled = false;
            isClicked = false;
        }

        public int InitialDelay
        {
            get
            {
                return m_initdelay;
            }
            set
            {
                m_initdelay = value;
                if (m_initdelay < 100)
                    m_initdelay = 100;
            }
        }

        public int RepeatDelay
        {
            get
            {
                return m_repdelay;
            }
            set
            {
                m_repdelay = value;
                if (m_repdelay < 100)
                    m_repdelay = 100;
            }
        }

    }
}
