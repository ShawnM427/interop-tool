﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Drawing.Drawing2D;

namespace InteropGenerator
{
    [ProvideProperty("Title", typeof(Control))]
    [ProvideProperty("ToolTip", typeof(Control))]
    class CustomTooltip : ToolTip
    {
        private Dictionary<Control, string> myTitles;
        private Dictionary<Control, string> myContents;
        private string myActiveTitle;
        private string myActiveText;

        public Font TitleFont
        {
            get;
            set;
        } = new Font(FontFamily.GenericSansSerif, 9.0f, FontStyle.Bold);
        public Font TextFont
        {
            get;
            set;
        } = new Font(FontFamily.GenericSansSerif, 8.0f, FontStyle.Italic);
        public Color TitleColor
        {
            get; set;
        } = SystemColors.ControlText;
        public Color TextColor
        {
            get; set;
        } = SystemColors.InfoText;
        public Color BorderColor
        {
            get; set;
        } = SystemColors.ActiveBorder;
        public Color BackColor2
        {
            get; set;
        } = SystemColors.ControlLightLight;

        public CustomTooltip()
        {
            myTitles = new Dictionary<Control, string>();
            myContents = new Dictionary<Control, string>();

            OwnerDraw = true;
            Popup += new PopupEventHandler(this.OnPopup);
            Draw += new DrawToolTipEventHandler(this.OnDraw);
        }

        public string GetTitle(Control context) {
            return myTitles.ContainsKey(context) ? myTitles[context] : null;
        }
        public void SetTitle(Control context, string title) {
            if (myTitles.ContainsKey(context))
                myTitles[context] = title;
            else
                myTitles.Add(context, title);
        }

        [Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
        new public string GetToolTip(Control context)
        {
            return myContents.ContainsKey(context) ? myContents[context] : null;
        }
        new public void SetToolTip(Control context, string text)
        {
            if (myContents.ContainsKey(context))
                myContents[context] = text;
            else
                myContents.Add(context, text);
            base.SetToolTip(context, "<text>");
        }

        private void OnPopup(object sender, PopupEventArgs e) // use this event to set the size of the tool tip
        {
            myActiveText = myContents.ContainsKey(e.AssociatedControl) ? myContents[e.AssociatedControl] : "";
            myActiveTitle = myTitles.ContainsKey(e.AssociatedControl) ? myTitles[e.AssociatedControl] : "";

            Size title = TextRenderer.MeasureText(myActiveTitle, TitleFont);
            Size text = TextRenderer.MeasureText(myActiveText, TextFont);
            e.ToolTipSize = new Size(Math.Max(title.Width, text.Width), title.Height + 8 + text.Height);
        }

        private void OnDraw(object sender, DrawToolTipEventArgs e) // use this event to customise the tool tip
        {
            Graphics g = e.Graphics;

            LinearGradientBrush b = new LinearGradientBrush(e.Bounds, BackColor, BackColor2, 45f);

            g.FillRectangle(b, e.Bounds);

            g.DrawRectangle(new Pen(new SolidBrush(BorderColor), 1), new Rectangle(e.Bounds.X, e.Bounds.Y,
                e.Bounds.Width - 1, e.Bounds.Height - 1));

            g.DrawString(myActiveTitle, TitleFont, new SolidBrush(TitleColor),
                new PointF(e.Bounds.X + 6, e.Bounds.Y + 6));
            Size title = TextRenderer.MeasureText(myActiveTitle, TitleFont);
            g.DrawLine(new Pen(new SolidBrush(BorderColor), 1), e.Bounds.X + 6, e.Bounds.Y + 6 + title.Height + 1, e.Bounds.Right - 6, e.Bounds.Y + 6 + title.Height + 1);
            g.DrawString(myActiveText, TextFont, new SolidBrush(TextColor),
                new PointF(e.Bounds.X + 8, e.Bounds.Y + 10 + title.Height)); // top layer

            b.Dispose();
        }
    }
}
