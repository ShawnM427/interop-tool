﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace InteropGenerator
{
    [DefaultEvent("ValueChanged")]
    public partial class CustomScroll : UserControl
    {
        private InputRepeater myDownRepeater;
        private InputRepeater myUpRepeater;

        public ScrollOrientation Orientation
        {
            get;
            set;
        }

        private float __value = 0;
        public float Value
        {
            get { return __value; }
            set { __value = value < 0 ? 0 : value > MaxValue ? MaxValue : value; Invalidate(); ValueChanged?.Invoke(this, new ScrollEventArgs(ScrollEventType.SmallDecrement, (int)Value)); }
        }
        private int __visibleSize = 0;
        public int VisibleSize
        {
            get { return __visibleSize; }
            set { __visibleSize = value; Invalidate(); }
        }
        private int __MaxValue = 0;
        public int MaxValue
        {
            get { return __MaxValue; }
            set { __MaxValue = value; Invalidate(); }
        }

        public int Offset
        {
            get { return (int)((MaxValue - VisibleSize) * Percentage); }
        }

        public float Percentage
        {
            get { return Value / (float)MaxValue; }
        }
        public float HandleScale
        {
            get { return MaxValue == 0 ? 1.0f : VisibleSize / (float)MaxValue; }
        }

        public event EventHandler<ScrollEventArgs> ValueChanged;

        public CustomScroll()
        {
            DoubleBuffered = true;
            ResizeRedraw = true;
            myDownRepeater = new InputRepeater();
            myDownRepeater.Fire += (o, e) => { Value += 20; };
            myUpRepeater = new InputRepeater();
            myUpRepeater.Fire += (o, e) => { Value -= 20; };

            MaxValue = Width;
            VisibleSize = Width / 2;
        }

        protected Rectangle GetScrollHandleBounds()
        {
            switch (Orientation)
            {
                case ScrollOrientation.HorizontalScroll:
                    {
                        int size = Bounds.Width - 2 * Bounds.Height;
                        int offset = Bounds.Height;

                        int unitScale = Math.Max((int)(size * HandleScale), Bounds.Height);
                        int center = offset + unitScale / 2 + (int)(Percentage * (size - unitScale));
                        int halfScale = unitScale / 2;

                        return new Rectangle(center - halfScale, 0, unitScale, Bounds.Height);
                    }

                case ScrollOrientation.VerticalScroll:
                    {
                        int size = Bounds.Height - 2 * Bounds.Width;
                        int offset = Bounds.Width;

                        int unitScale = Math.Max((int)(size * HandleScale), Bounds.Width);
                        int center = offset + unitScale / 2 + (int)(Percentage * (size - unitScale));
                        int halfScale = unitScale / 2;

                        return new Rectangle(0, center - halfScale, Bounds.Width, unitScale);
                    }
                default:
                    return new Rectangle();
            }
        }

        private Rectangle GetUpRectangle()
        {
            switch (Orientation) {
                case ScrollOrientation.HorizontalScroll:
                    return new Rectangle(0, 0, Bounds.Height, Bounds.Height);
                case ScrollOrientation.VerticalScroll:
                    return new Rectangle(0, 0, Bounds.Width, Bounds.Width);
                default:
                    return new Rectangle();
            }
        }

        private Rectangle GetDownRectangle()
        {
            switch (Orientation)
            {
                case ScrollOrientation.HorizontalScroll:
                    return new Rectangle(Bounds.Right - Bounds.Height, 0, Bounds.Height, Bounds.Height);
                case ScrollOrientation.VerticalScroll:
                    return new Rectangle(0, Bounds.Bottom - Bounds.Width, Bounds.Width, Bounds.Width);
                default:
                    return new Rectangle();
            }
        }

        protected void DrawArrow(Graphics g, Pen p, Rectangle bounds, float angle)
        {
            float scaleX = bounds.Width / 2.75f;
            Point center = new Point((bounds.Left + bounds.Right) / 2, (bounds.Top + bounds.Bottom) / 2);
            Matrix store = g.Transform;
            g.TranslateTransform(center.X, center.Y);
            g.RotateTransform(angle);
            g.TranslateTransform(0, scaleX / 2.0f);
            g.DrawLine(p, - scaleX, 0, 0, - scaleX);
            g.DrawLine(p, + scaleX, 0, 0, - scaleX);
            g.Transform = store;
        }


        protected override void OnPaint(PaintEventArgs e)
        {
            Rectangle handleBounds = GetScrollHandleBounds();

            Pen fore = Theme.Active.Pen("Fore");
            Pen border = Theme.Active.Pen("Border");

            e.Graphics.DrawRectangle(border, GetUpRectangle());
            DrawArrow(e.Graphics, fore, GetUpRectangle(), Orientation == ScrollOrientation.HorizontalScroll ? -90.0f : 0);
            e.Graphics.DrawRectangle(border, GetDownRectangle());
            DrawArrow(e.Graphics, fore, GetDownRectangle(), Orientation == ScrollOrientation.HorizontalScroll ? 90.0f : 180.0f);

            e.Graphics.FillRectangle(Theme.Active.Brush("BackDark"), Bounds);
            e.Graphics.DrawRectangle(border, new Rectangle(Point.Empty, Size));
            e.Graphics.FillRectangle(Theme.Active.Brush("BackLight"), handleBounds);
            e.Graphics.DrawRectangle(border, handleBounds);

            base.OnPaint(e);
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            if (Capture) {
                Value += (e.Delta / 60.0f) * HandleScale;
                Value = Value < 0 ? 0 : Value > MaxValue ? MaxValue : Value;
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (Capture)
            {
                switch (Orientation)
                {
                    case ScrollOrientation.HorizontalScroll:
                        {
                            int size = Bounds.Width - 2 * Bounds.Height;
                            int offset = Bounds.Height;

                            int unitScale = Math.Max((int)(size * HandleScale), Bounds.Height);
                            int center = offset + unitScale / 2 + (int)(Percentage * (size - unitScale));
                            float percentage = ((e.X - offset - unitScale / 2) / (float)(size - unitScale));
                            percentage = percentage < 0 ? 0.0f : percentage > 1.0f ? 1.0f : percentage;
                            Value = MaxValue * percentage;
                        }
                        break;
                    case ScrollOrientation.VerticalScroll:
                        {
                            int size = Bounds.Height - 2 * Bounds.Width;
                            int offset = Bounds.Width;

                            int unitScale = Math.Max((int)(size * HandleScale), Bounds.Width);
                            int center = offset + unitScale / 2 + (int)(Percentage * (size - unitScale));
                            float percentage = ((e.Y - offset - unitScale / 2) / (float)(size - unitScale));
                            percentage = percentage < 0 ? 0.0f : percentage > 1.0f ? 1.0f : percentage;
                            Value = MaxValue * percentage;
                        }
                        break;
                }
            }
        }
        
        protected override void OnMouseClick(MouseEventArgs e) {
            if (!GetUpRectangle().Contains(e.X, e.Y) && !GetDownRectangle().Contains(e.X, e.Y))
                Capture = true;
        }

        protected override void OnMouseDown(MouseEventArgs e) {
            if (GetUpRectangle().Contains(e.X, e.Y))
                myUpRepeater.MouseDown(this, e);
            else if (GetDownRectangle().Contains(e.X, e.Y))
                myDownRepeater.MouseDown(this, e);
        }

        protected override void OnMouseUp(MouseEventArgs e) {
            myUpRepeater.Released(this, e);
            myDownRepeater.Released(this, e);
            Capture = false;
        }
    }
}
