﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InteropGenerator
{
    class ThemedComboBox : ComboBox
    {
    //    decimal __Value;
    //    public decimal Value
    //    {
    //        get { return __Value; }
    //        set { __Value = value; Invalidate(); }
    //    }
    //    int __DecimalPlaces;
    //    public int DecimalPlaces
    //    {
    //        get { return __DecimalPlaces; }
    //        set { __DecimalPlaces = value; Invalidate(); }
    //    }

        public ThemedComboBox() {
            
            SetStyle(ControlStyles.UserPaint | ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
            DrawMode = DrawMode.OwnerDrawFixed;
        }

        protected void DrawArrow(Graphics g, Pen p, Rectangle bounds, float angle)
        {
            float scaleX = bounds.Width / 2.75f;
            Point center = new Point((bounds.Left + bounds.Right) / 2, (bounds.Top + bounds.Bottom) / 2);
            Matrix store = g.Transform;
            g.TranslateTransform(center.X, center.Y);
            g.RotateTransform(angle);
            g.TranslateTransform(0, scaleX / 2.0f);
            g.DrawLine(p, -scaleX, 0, 0, -scaleX);
            g.DrawLine(p, +scaleX, 0, 0, -scaleX);
            g.Transform = store;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Rectangle localBounds = new Rectangle(0, 0, Width - 1, Height - 1);
            e.Graphics.FillRectangle(Theme.Active.Brush("Back"), localBounds);
            e.Graphics.DrawRectangle(Theme.Active.Pen("Fore"), localBounds);

            Rectangle dropdown = new Rectangle(Width - Height, 0, Height, Height);

            DrawArrow(e.Graphics, Theme.Active.Pen("Fore"), dropdown, -180.0f);

            if (SelectedIndex >= 0) {
                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Near;
                format.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString(Items[SelectedIndex].ToString(), Font, Theme.Active.Brush("Fore"), localBounds, format);
            }
            //e.Graphics.DrawRectangle()
        }

        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            if (e.Index >= 0)
            {
                e.Graphics.Clip = new Region(e.Bounds);
                e.Graphics.Clear(Theme.Active.Colors["Back"]);
                e.Graphics.DrawRectangle(Theme.Active.Pen("Border"), e.Bounds);
                e.Graphics.DrawString(Items[e.Index].ToString(), Font, Theme.Active.Brush("Fore"), e.Bounds);
                //base.OnDrawItem(e);
            }
        }
    }
}
