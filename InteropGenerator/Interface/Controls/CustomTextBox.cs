﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InteropGenerator
{
    class CustomTextBox : Panel {

        //private Bitmap myImageBuffer;
        //private Graphics myBufferGraphics;
        //private Graphics myHostGraphics;
        
        private CustomScroll hScroll;
        private CustomScroll vScroll;
        private TextBox      myText;

        public Padding InnerPadding
        {
            get;
            set;
        } = new Padding(4);
        
        public string Text
        {
            get { return myText.Text; }
            set { myText.Text = value; }
        }
        public bool ReadOnly
        {
            get { return myText.ReadOnly; }
            set {
                myText.ReadOnly = value;
                myText.BackColor = myText.ReadOnly ? Theme.Active.Colors["BackLight"] : Theme.Active.Colors["Back"];
                BackColor = myText.BackColor;
            }
        }
        
        public string[] Lines
        {
            get { return myText.Lines; }
            set { myText.Lines = value; }
        }
        public string GhostText
        {
            get;
            set;
        }
        public StringAlignment GhostAlignment
        {
            get;
            set;
        }

        public CustomTextBox()
        {
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            ResizeRedraw = true;

            MouseWheelRedirector.Attach(this);

            hScroll = new CustomScroll();
            vScroll = new CustomScroll();

            DoubleBuffered = true;

            BorderStyle = BorderStyle.FixedSingle;
            ForeColor = Theme.Active.Colors["Border"];

            //Scroll
            
            myText = new TextBox();
            myText.Dock = DockStyle.Fill;
            myText.Multiline = true;
            myText.WordWrap = false;
            myText.BorderStyle = BorderStyle.None;
            myText.TextChanged += OnSubTextChanged;
            myText.BackColorChanged += (o, e) => {
                Color c = myText.ReadOnly ? Theme.Active.Colors["BackLight"] : Theme.Active.Colors["Back"];
                if (myText.BackColor != c) {
                    myText.BackColor = c;
                    BackColor = c;
                }
            };
            myText.LostFocus += (o, e) => { Invalidate(); };
            
            hScroll.Height = 18;
            hScroll.Dock = DockStyle.Bottom;
            hScroll.Orientation = ScrollOrientation.HorizontalScroll;
            hScroll.Visible = false;
            hScroll.ValueChanged += ScrollChanged;

            vScroll.Width = 18;
            vScroll.Dock = DockStyle.Right;
            vScroll.Orientation = ScrollOrientation.VerticalScroll;
            vScroll.Visible = false;
            vScroll.ValueChanged += ScrollChanged;

            Controls.Add(hScroll);
            Controls.Add(vScroll);
            Controls.Add(myText);
            PerformLayout();
            myText.Dock = DockStyle.None;
        }

        private void MouseWheelChanged(object sender, MouseEventArgs e)
        {
            if (e.X < Width && e.Y < Height)
                OnMouseWheel(e);
        }

        public void BindTabSize(INotifyPropertyChanged notifier, string propName) {
            myText.BindTabSize(notifier, propName);
        }

        private void ScrollChanged(object sender, ScrollEventArgs e) {
            myText.Location = new Point(InnerPadding.Left - hScroll.Offset, InnerPadding.Left - vScroll.Offset);
            Invalidate();
        }

        protected override void OnSizeChanged(EventArgs e) {
            HandleResizing();
            base.OnSizeChanged(e);
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            hScroll.Value += (e.Delta / 60.0f) * hScroll.HandleScale;
            base.OnMouseWheel(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.X > Bounds.Width - (vScroll.Visible ? vScroll.Width : 0))
                Cursor.Current = Cursors.Arrow;
            else if (e.Y > Bounds.Height - (hScroll.Visible ? hScroll.Height : 0))
                Cursor.Current = Cursors.Arrow;
        }

        protected void HandleResizing()
        {
            Size size = new Size();
            size.Width = myText.PreferredSize.Width < Width ? Width : myText.PreferredSize.Width + 4;
            size.Height = myText.PreferredSize.Height < Height ? Height : myText.PreferredSize.Height + 4;
            myText.Location = new Point(InnerPadding.Left - hScroll.Offset, InnerPadding.Top - vScroll.Offset);
            myText.Size = size;

            if (Width - 32 < myText.PreferredSize.Width)
            {
                hScroll.Visible = true;
                hScroll.Enabled = true;
            }
            else
            {
                hScroll.Visible = false;
                hScroll.Enabled = false;
            }

            if (Height - 32 < myText.PreferredSize.Height)
            {
                vScroll.Visible = true;
                vScroll.Enabled = true;
            }
            else
            {
                vScroll.Visible = false;
                vScroll.Enabled = false;
            }

            hScroll.MaxValue = size.Width;
            hScroll.VisibleSize = Width - (vScroll.Visible ? vScroll.Width * 2 : 0) - InnerPadding.Right;

            vScroll.MaxValue = size.Height;
            vScroll.VisibleSize = Height - (hScroll.Visible ? hScroll.Height : 0) - InnerPadding.Bottom;
        }

        protected void OnSubTextChanged(object sender, EventArgs e) {
            HandleResizing();
        }
    }
}
