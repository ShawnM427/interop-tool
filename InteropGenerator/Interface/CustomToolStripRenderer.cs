﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InteropGenerator
{
    public class CustomToolStripRenderer : ToolStripRenderer
    {
        protected override void Initialize(ToolStrip toolStrip)
        {
            toolStrip.BackColor = Theme.Active.Colors["Back"];
            toolStrip.ForeColor = Theme.Active.Colors["Fore"];

            foreach(ToolStripItem i in toolStrip.Items) {
                InitializeItem(i);
            }
        }
        
        protected override void InitializeItem(ToolStripItem item)
        {
            item.BackColor = Theme.Active.Colors["Back"];
            item.ForeColor = Theme.Active.Colors["Fore"];

            if (item is ToolStripMenuItem)
            {
                item.MouseEnter += (o, e) =>
                {
                    item.ForeColor = Theme.Active.Colors["ToolStrip.Hover.Fore"];
                    item.Tag = "Hover";
                };
                item.MouseLeave += (o, e) =>
                {
                    item.ForeColor = Theme.Active.Colors["Fore"];
                    item.Tag = "";
                };
            }

            if (item is ToolStripMenuItem) {
                ToolStripMenuItem menu = item as ToolStripMenuItem;
                foreach(ToolStripItem child in menu.DropDownItems) {
                    InitializeItem(child);
                }
            } else if (item is ToolStripDropDownButton) {
                ToolStripDropDownButton drop = item as ToolStripDropDownButton;
                foreach (ToolStripItem i in drop.DropDownItems) {
                    InitializeItem(i);
                }
            } else if (item is ToolStripSplitButton) {
                ToolStripSplitButton button = item as ToolStripSplitButton;
                foreach (ToolStripItem i in button.DropDownItems) {
                    InitializeItem(i);
                }
            }
        }

        protected override void OnRenderMenuItemBackground(ToolStripItemRenderEventArgs e) {
            Pen myBorderPen = Theme.Active.Pen("Border");

            Brush myBackLightBrush = Theme.Active.Brush("BackLight");
            Brush myBorderBrush = Theme.Active.Brush("Border");
            Brush myBackBrush = Theme.Active.Brush("Back");

            if ((string)e.Item.Tag == "Hover")
                e.Graphics.FillRectangle(myBackLightBrush, new Rectangle(0, 0, e.Item.Width - 1, e.Item.Height - 1));
            else
                e.Graphics.FillRectangle(myBackBrush, new Rectangle(0, 0, e.Item.Width - 1, e.Item.Height - 1));
            e.Graphics.DrawRectangle(myBorderPen, new Rectangle(0, 0, e.Item.Width - 1, e.Item.Height - 1));
        }
        protected override void OnRenderToolStripBackground(ToolStripRenderEventArgs e) {
            Brush myBackBrush = Theme.Active.Brush("Back");
            e.Graphics.FillRectangle(myBackBrush, e.AffectedBounds);
        }
        protected override void OnRenderToolStripBorder(ToolStripRenderEventArgs e) {
            Pen myBorderPen = Theme.Active.Pen("Border");
            e.Graphics.DrawRectangle(myBorderPen, e.AffectedBounds);
        }
    }
}
