﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace InteropGenerator
{
    public partial class ThemedForm : Form
    {
        private int cCaption = 16;
        private int cGrip = 4;

        new public bool MaximizeBox
        {
            get { return base.MaximizeBox; }
            set { base.MaximizeBox = value; btnMaximize.Visible = value; }
        }
        new public bool MinimizeBox
        {
            get { return base.MinimizeBox; }
            set { base.MinimizeBox = value; btnMinimize.Visible = value; }
        }
        new public bool ShowIcon
        {
            get { return base.ShowIcon; }
            set { base.ShowIcon = value; pctIcon.Visible = value; }
        }
        new public Icon Icon
        {
            get { return base.Icon; }
            set { base.Icon = value; pctIcon.Image = value.ToBitmap(); }
        }
        string __Text;
        public override string Text
        {
            get => __Text;
            set => __Text = lblTitle.Text = value;
        }

        public ThemedForm()
        {
            InitializeComponent();
            pnlContent.Layout += (o, e) => OnLayout(e);
        }

        // During OnResize, call RedrawWindow with Frame|UpdateNow|Invalidate so that the frame is always redrawn accordingly
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            if (DesignMode)
            {
                RecreateHandle();
            }
            Native.RedrawWindow(this.Handle, IntPtr.Zero, IntPtr.Zero, Native.RedrawWindowFlags.Frame | Native.RedrawWindowFlags.UpdateNow | Native.RedrawWindowFlags.Invalidate);
        }

        // Make sure that WS_BORDER is a style, otherwise borders aren't painted at all
        protected override CreateParams CreateParams
        {
            get
            {
                if (DesignMode)
                {
                    return base.CreateParams;
                }
                CreateParams cp = base.CreateParams;
                cp.ExStyle &= Native.WS_EX_CLIENTEDGE; // WS_EX_CLIENTEDGE
                cp.Style |= Native.WS_BORDER; // WS_BORDER
                return cp;
            }
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0x84)
            {  // Trap WM_NCHITTEST
                Point pos = new Point(m.LParam.ToInt32());
                pos = this.PointToClient(pos);
                if (pos.Y < cCaption)
                {
                    m.Result = (IntPtr)2;  // HTCAPTION
                    return;
                }
                if (pos.X >= this.ClientSize.Width - cGrip && pos.Y >= this.ClientSize.Height - cGrip)
                {
                    m.Result = (IntPtr)17; // HTBOTTOMRIGHT
                    return;
                }
            } else if (m.Msg == 0x85) // WM_NCPAINT
            {
                //if (BorderStyle == BorderStyle.None) {
                //    return;
                //}

                IntPtr hDC = Native.GetWindowDC(m.HWnd);
                using (Graphics g = Graphics.FromHdc(hDC))
                {
                    g.DrawRectangle(Theme.Active.Pen(5.0f, PenAlignment.Inset, "Form.Border", "Border"),
                        new Rectangle(Point.Empty, Size));
                }
                Native.ReleaseDC(m.HWnd, hDC);
            }
            base.WndProc(ref m);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            e.Graphics.DrawRectangle(Theme.Active.Pen(5.0f, PenAlignment.Inset, "Form.Border", "Border"),
                new Rectangle(Point.Empty, Size));
        }
        
        protected override void OnLayout(LayoutEventArgs levent)
        {
            base.OnLayout(levent);

            PropegateBaseTheme(this);
        }
        
        protected void PropegateBaseTheme(Control control)
        {
            if (Site != null && Site.DesignMode) return;

            if (control != null)
            {
                Theme.ThemeModified += (e) => { ThemeFactory.ApplyTheme(control, Theme.Active); };
                ThemeFactory.ApplyTheme(control, Theme.Active);

                foreach (Control child in control.Controls)
                {
                    PropegateBaseTheme(child);
                }
            }
        }


        private void btnClose_Click(object sender, EventArgs e) {
            Close();
        }

        private void btnMaximize_Click(object sender, EventArgs e){
            WindowState = WindowState == FormWindowState.Normal ? FormWindowState.Maximized : FormWindowState.Normal;
            btnMaximize.Text = WindowState == FormWindowState.Normal ? "🗖" : "🗗";
        }

        private void pnlHeader_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Native.ReleaseCapture();
                Native.SendMessage(Handle, Native.WM_NCLBUTTONDOWN, Native.HT_CAPTION, 0);
            }
        }

        private void btnMinimize_Click(object sender, EventArgs e) {
            WindowState = FormWindowState.Minimized;
        }
    }
}
