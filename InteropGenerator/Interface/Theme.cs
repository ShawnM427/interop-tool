﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InteropGenerator
{
    public class Theme : INotifyPropertyChanged
    {
        public delegate void ThemeChanged(string propName);

        public class ColorPalette : INotifyPropertyChanged
        {
            public Color DefaultColor = Color.Transparent;
            private Dictionary<string, Color> myColors;
            public ColorPalette() { myColors = new Dictionary<string, Color>(); }

            public Color this[string name, params string[] fallbacks]
            {
                get {
                    if (myColors.ContainsKey(name))
                        return myColors[name];
                    else {
                        foreach (string s in fallbacks)
                            if (myColors.ContainsKey(s))
                                return myColors[s];

                        return DefaultColor;
                    }
                }
                set { if (myColors.ContainsKey(name)) myColors[name] = value; else myColors.Add(name, value); PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name)); }
            }

            public event PropertyChangedEventHandler PropertyChanged;
        }

        public class FontSet : INotifyPropertyChanged
        {
            public Font DefaultFont = new Font(FontFamily.GenericSansSerif, 9.25f);
            private Dictionary<string, Font> myFonts;
            public FontSet() { myFonts = new Dictionary<string, Font>(); }

            public Font this[string name, params string[] fallbacks]
            {
                get
                {
                    if (myFonts.ContainsKey(name))
                        return myFonts[name];
                    else
                    {
                        foreach (string s in fallbacks)
                            if (myFonts.ContainsKey(s))
                                return myFonts[s];

                        return DefaultFont;
                    }
                }
                set { if (myFonts.ContainsKey(name)) myFonts[name] = value; else myFonts.Add(name, value); PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name)); }
            }

            public event PropertyChangedEventHandler PropertyChanged;
        }

        public ColorPalette Colors { get; }
        public FontSet Fonts { get; }

        public Pen Pen(string name, params string[] fallbacks) {
            return new Pen(Brush(name));
        }
        public Pen Pen(float width, string name, params string[] fallbacks) {
            return new Pen(Brush(name), width);
        }
        public Pen Pen(float width, PenAlignment alignment, string name, params string[] fallbacks)
        {
            return new Pen(Brush(name), width) { Alignment = alignment };
        }
        public Brush Brush(string name, params string[] fallbacks) {
            return new SolidBrush(Colors[name, fallbacks]);
        }

        public Theme()
        {
            Colors = new ColorPalette();
            Fonts = new FontSet();
            Colors.PropertyChanged += (o, p) => { PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("[Color]" + p)); };
            Fonts.PropertyChanged += (o, p) => { PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("[Font]" + p)); };

            __InitDefaultTheme();
        }
        
        private void __InitDefaultTheme() {
            Colors["Back"] = Color.FromArgb(42, 42, 42);
            Colors["Fore"] = Color.FromArgb(200, 200, 200);
            Colors["Selection.Back"] = Color.FromArgb(65, 65, 65);
            Colors["Selection.Fore"] = Color.FromArgb(220, 220, 220);
            Colors["BackLight"] = Color.FromArgb(50, 50, 50);
            Colors["BackDark"] = Color.FromArgb(20, 20, 20);
            Colors["Border"] = Color.FromArgb(64, 64, 64);

            Colors["CustomTextBox.Back"] = Color.FromArgb(46, 46, 46);

            Colors["ToolStrip.Hover.Fore"] = Color.FromArgb(230, 230, 230);

            Colors["ToolTip.Back"] = new Color();
            Colors["ToolTip.Fore"] = new Color();

            Fonts["Default"] = new Font(FontFamily.GenericSansSerif, 9.25f);
            Fonts["ToolTip.Header"] = new Font(FontFamily.GenericSansSerif, 9.25f, FontStyle.Bold);
            Fonts["ToolTip.Content"] = new Font(FontFamily.GenericSansSerif, 8.25f, FontStyle.Italic);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private static Theme __ActiveTheme = new Theme();
        public static Theme Active
        {
            get { return __ActiveTheme; }
            set
            {
                if (__ActiveTheme != null)
                    __ActiveTheme.PropertyChanged -= __ModifiedProxy;
                __ActiveTheme = value;
                __ActiveTheme.PropertyChanged += __ModifiedProxy;
                ThemeModified?.Invoke("ActiveTheme");
            }
        }

        private static void __ModifiedProxy(object sender, PropertyChangedEventArgs e) {
            ThemeModified?.Invoke(e.PropertyName);
        }

        public static event ThemeChanged ThemeModified;
    }
}
