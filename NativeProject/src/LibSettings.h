#pragma once

#ifdef DLL_EXPORTS
#define LIB_API __declspec(dllexport)
#elif DLL_IMPORTS
#define  LIB_API __declspec(dllimport)
#else
#define LIB_API
#endif