#include "Header.h"

//TroopInstance
LIB_API TroopInstance* __Native_TroopInstance_TroopInstance(SharedStats*shared) { 
	return new TroopInstance(shared);
}
LIB_API TroopInstance* __Native_TroopInstance_TroopInstance(SharedStats*shared, TroopInstance*cloneSource) { 
	return new TroopInstance(shared,cloneSource);
}
LIB_API uint32_t __Native_TroopInstance_GetHealth(TroopInstance*ctx) { 
	return ctx->GetHealth();
}
LIB_API void __Native_TroopInstance_SetHealth(TroopInstance*ctx, uint32_tval) { 
	ctx->SetHealth(val);
}
LIB_API void __Native_TroopInstance_ModifyHealth(TroopInstance*ctx, intchange) { 
	ctx->ModifyHealth(change);
}
LIB_API float __Native_TroopInstance_GetHealthPercentage(TroopInstance*ctx) { 
	return ctx->GetHealthPercentage();
}
LIB_API uint32_t __Native_TroopInstance_GetLevel(TroopInstance*ctx) { 
	return ctx->GetLevel();
}
LIB_API void __Native_TroopInstance_SetLevel(TroopInstance*ctx, uint32_tval) { 
	ctx->SetLevel(val);
}
LIB_API uint32_t __Native_TroopInstance_GetMaxHealth(TroopInstance*ctx) { 
	return ctx->GetMaxHealth();
}
LIB_API uint32_t __Native_TroopInstance_GetAttack(TroopInstance*ctx) { 
	return ctx->GetAttack();
}
LIB_API uint32_t __Native_TroopInstance_GetDefense(TroopInstance*ctx) { 
	return ctx->GetDefense();
}
LIB_API float __Native_TroopInstance_GetMovementSpeed(TroopInstance*ctx) { 
	return ctx->GetMovementSpeed();
}
LIB_API float __Native_TroopInstance_GetAttackSpeed(TroopInstance*ctx) { 
	return ctx->GetAttackSpeed();
}
LIB_API float __Native_TroopInstance_GetAttackRange(TroopInstance*ctx) { 
	return ctx->GetAttackRange();
}
LIB_API size_t __Native_TroopInstance_GetTypeID(TroopInstance*ctx) { 
	return ctx->GetTypeID();
}

//TroopFactory
LIB_API TroopInstance* __Native_TroopFactory_Create(TroopFactory*ctx, SharedStats*shared) { 
	return ctx->Create(shared);
}
LIB_API TroopInstance* __Native_TroopFactory_Create(TroopFactory*ctx, SharedStats*shared, TroopInstance*cloneSource) { 
	return ctx->Create(shared,cloneSource);
}
LIB_API void __Native_TroopFactory_Free(TroopFactory*ctx, TroopInstance*troop) { 
	ctx->Free(troop);
}

