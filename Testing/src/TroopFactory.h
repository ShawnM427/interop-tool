#include "LibSettings.h"

#ifdef __cplusplus
extern "C" {
#endif
//TroopInstance
LIB_API void __Native_TroopInstance__DTOR();
LIB_API TroopInstance* __Native_TroopInstance_TroopInstance(SharedStats*shared);
LIB_API TroopInstance* __Native_TroopInstance_TroopInstance(SharedStats*shared, TroopInstance*cloneSource);
LIB_API uint32_t __Native_TroopInstance_GetHealth(TroopInstance*ctx);
LIB_API void __Native_TroopInstance_SetHealth(TroopInstance*ctx, uint32_tval);
LIB_API void __Native_TroopInstance_ModifyHealth(TroopInstance*ctx, intchange);
LIB_API float __Native_TroopInstance_GetHealthPercentage(TroopInstance*ctx);
LIB_API uint32_t __Native_TroopInstance_GetLevel(TroopInstance*ctx);
LIB_API void __Native_TroopInstance_SetLevel(TroopInstance*ctx, uint32_tval);
LIB_API uint32_t __Native_TroopInstance_GetMaxHealth(TroopInstance*ctx);
LIB_API uint32_t __Native_TroopInstance_GetAttack(TroopInstance*ctx);
LIB_API uint32_t __Native_TroopInstance_GetDefense(TroopInstance*ctx);
LIB_API float __Native_TroopInstance_GetMovementSpeed(TroopInstance*ctx);
LIB_API float __Native_TroopInstance_GetAttackSpeed(TroopInstance*ctx);
LIB_API float __Native_TroopInstance_GetAttackRange(TroopInstance*ctx);
LIB_API size_t __Native_TroopInstance_GetTypeID(TroopInstance*ctx);

//TroopFactory
LIB_API void __Native_TroopFactory__DTOR();
LIB_API TroopInstance* __Native_TroopFactory_Create(TroopFactory*ctx, SharedStats*shared);
LIB_API TroopInstance* __Native_TroopFactory_Create(TroopFactory*ctx, SharedStats*shared, TroopInstance*cloneSource);
LIB_API void __Native_TroopFactory_Free(TroopFactory*ctx, TroopInstance*troop);

#ifdef __cplusplus
}
#endif
