#include "LibSettings.h"

#ifdef __cplusplus
extern "C" {
#endif
//TroopStatFactory
LIB_API SharedStats* __Native_TroopStatFactory_CreateStats(TroopStatFactory*ctx, uint32_tmaxHealth, uint32_tattack, uint32_tdefense, floatmovementSpeed, floatattackSpeed, floatattackRange);
LIB_API TroopStatFactory* __Native_TroopStatFactory_if(stats.MaxHealth, tats.Attack, tats.Defense, tats.MovementSpeed, tats.AttackSpeed, tats.AttackRange, attackRange);

#ifdef __cplusplus
}
#endif
